import 'package:app_margarita_duran/bloc/authenticationBloc.dart';
import 'package:app_margarita_duran/controllers/homeController.dart';
import 'package:app_margarita_duran/controllers/reservationController.dart';
import 'package:app_margarita_duran/events/authenticationEvents.dart';
import 'package:app_margarita_duran/models/sedeModel.dart';
import 'package:app_margarita_duran/pages/loginPage.dart';
import 'package:app_margarita_duran/pages/splashPage.dart';
import 'package:app_margarita_duran/repositories/userRepository.dart';
import 'package:app_margarita_duran/states/authenticationStates.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AuthenticationBloc _authenticationBloc;

  @override
  void initState() {
    _authenticationBloc = AuthenticationBloc(userRepository: UserRepository());
    _authenticationBloc.add(AppStarted());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => _authenticationBloc,
      child: WillPopScope(
        onWillPop: () => Future.value(false),
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            home: BlocListener<AuthenticationBloc, AuthenticationState>(
              cubit: _authenticationBloc,
              listener: (BuildContext context, state) {},
              child: BlocBuilder(
                  cubit: _authenticationBloc,
                  //ignore: missing_return
                  builder: (BuildContext context, AuthenticationState state) {
                    return AnimatedSwitcher(
                      duration: Duration(milliseconds: 250),
                      switchOutCurve: Threshold(0.05),
                      transitionBuilder:
                          (Widget child, Animation<double> animation) {
                        return SlideTransition(
                          position: Tween<Offset>(
                            begin: const Offset(0.25, 0),
                            end: Offset.zero,
                          ).animate(animation),
                          child: child,
                        );
                      },
                      child: _buildPage(context, state),
                    );
                  }),
            )),
      ),
    );
  }

  _buildPage(BuildContext context, AuthenticationState state) {
    if (state is AuthenticationUninitialized) {
      return SplashPage(
        sede: _authenticationBloc.sede ??
            Sede(
                address:
                    "Calle daniel hernandez y 4ta av. Sur sta. Tecla, SANTA TECLA, LA LIBERTAD.",
                code: 'E0108',
                logo: 'assets/images/badge.png',
                id: '1',
                name: 'Margarita Duran'),
      );
    } else if (state is AuthenticationAuthenticated) {
      return HomeController(
        authenticationBloc: _authenticationBloc,
      );
    } else if (state is AuthenticationUnauthenticated) {
      return LoginPage(
        sede: state.sede,
        authenticationBloc: _authenticationBloc,
      );
    } else if (state is AuthenticationLoading) {
      return Scaffold(
        backgroundColor: Colors.white,
        body: LoadingIndicator(),
      );
    } else if (state is NuevaMatriculaOptions) {
      return ReservationController(authenticationBloc: _authenticationBloc);
    }
  }
}
