import 'package:app_margarita_duran/bloc/authenticationBloc.dart';
import 'package:app_margarita_duran/bloc/homeBloc.dart';
import 'package:app_margarita_duran/events/homeEvents.dart';
import 'package:app_margarita_duran/global/styles.dart';
import 'package:app_margarita_duran/global/widgets.dart';
import 'package:app_margarita_duran/pages/homePage.dart';
import 'package:app_margarita_duran/pages/profilePage.dart';
import 'package:app_margarita_duran/pages/reposicionPage.dart';
import 'package:app_margarita_duran/pages/splashPage.dart';
import 'package:app_margarita_duran/states/homeStates.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HomeController extends StatefulWidget {
  final AuthenticationBloc authenticationBloc;

  const HomeController({Key key, @required this.authenticationBloc})
      : super(key: key);
  @override
  _HomeControllerState createState() => _HomeControllerState();
}

class _HomeControllerState extends State<HomeController> {
  AuthenticationBloc get authenticationBloc => widget.authenticationBloc;
  HomeBloc _homeBloc;

  @override
  void dispose() {
    _homeBloc.close();
    super.dispose();
  }

  @override
  void initState() {
    _homeBloc = HomeBloc(OnHomePage(authenticationBloc.student),
        authenticationBloc: authenticationBloc,
        userRepository: authenticationBloc.userRepository);
    _homeBloc.add(ToHomePage(student: authenticationBloc.student));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => _homeBloc,
      child: WillPopScope(
        onWillPop: () {
          Fluttertoast.showToast(
              msg:
                  'Por favor utilice el menu lateral en la esquina superior izquierda para navegar dentro de la aplicación',
              backgroundColor: MyColors.mainColor);
          return Future.value(false);
        },
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            home: BlocListener<HomeBloc, HomeState>(
              cubit: _homeBloc,
              listener: (BuildContext context, state) {
                if (state is FirstTimeInfoForm) {
                  return GlobalWidgets.showInfoForm(context, _homeBloc);
                }
              },
              child: BlocBuilder(
                  cubit: _homeBloc,
                  //ignore: missing_return
                  builder: (BuildContext context, HomeState state) {
                    return AnimatedSwitcher(
                      duration: Duration(milliseconds: 250),
                      switchOutCurve: Threshold(0.05),
                      transitionBuilder:
                          (Widget child, Animation<double> animation) {
                        return SlideTransition(
                          position: Tween<Offset>(
                            begin: const Offset(0.25, 0),
                            end: Offset.zero,
                          ).animate(animation),
                          child: child,
                        );
                      },
                      child: _buildPage(context, state),
                    );
                  }),
            )),
      ),
    );
  }

  _buildPage(BuildContext context, HomeState state) {
    if (state is OnHomePage) {
      return HomePage(
        authenticationBloc: authenticationBloc,
        homeBloc: _homeBloc,
        student: state.student,
      );
    } else if (state is OnRecuperationPage) {
      return ReposicionPage(
          authenticationBloc: authenticationBloc,
          homeBloc: _homeBloc,
          module: state.module);
    } else if (state is OnStudentProfilePage) {
      return ProfilePage(
          student: state.student,
          authenticationBloc: authenticationBloc,
          homeBloc: _homeBloc);
    } else {
      return SplashPage(
        sede: authenticationBloc.sede,
      );
    }
  }
}
