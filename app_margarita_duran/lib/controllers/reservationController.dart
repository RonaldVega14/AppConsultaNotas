import 'package:app_margarita_duran/bloc/authenticationBloc.dart';
import 'package:app_margarita_duran/bloc/reservationBloc.dart';
import 'package:app_margarita_duran/events/reservationEvents.dart';
import 'package:app_margarita_duran/pages/nuevoIngresoPage.dart';
import 'package:app_margarita_duran/pages/reservarMatricula.dart';
import 'package:app_margarita_duran/repositories/userRepository.dart';
import 'package:app_margarita_duran/states/reservationStates.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ReservationController extends StatefulWidget {
  final AuthenticationBloc authenticationBloc;

  const ReservationController({Key key, @required this.authenticationBloc})
      : super(key: key);
  @override
  _ReservationControllerState createState() => _ReservationControllerState();
}

class _ReservationControllerState extends State<ReservationController> {
  ReservationBloc _reservationBloc;
  AuthenticationBloc get authenticationBloc => widget.authenticationBloc;

  @override
  void initState() {
    _reservationBloc = ReservationBloc(
        userRepository: UserRepository(), sede: authenticationBloc.sede);
    _reservationBloc.add(ReservationStarted());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => _reservationBloc,
      child: WillPopScope(
        onWillPop: () => Future.value(false),
        child: BlocListener<ReservationBloc, ReservationState>(
          cubit: _reservationBloc,
          listener: (BuildContext context, ReservationState state) {
            //TODO: Implement things that are only supposed to be called once.
          },
          child: BlocBuilder(
              cubit: _reservationBloc,
              builder: (BuildContext context, ReservationState state) {
                return AnimatedSwitcher(
                  duration: Duration(milliseconds: 250),
                  switchOutCurve: Threshold(0.05),
                  transitionBuilder:
                      (Widget child, Animation<double> animation) {
                    return SlideTransition(
                      position: Tween<Offset>(
                              begin: const Offset(0.25, 0), end: Offset.zero)
                          .animate(animation),
                      child: child,
                    );
                  },
                  child: _buildPage(context, state),
                );
              }),
        ),
      ),
    );
  }

  _buildPage(BuildContext context, ReservationState state) {
    if (state is HomeReservation) {
      return NuevoIngresoPage(
        authenticationBloc: authenticationBloc,
        reservationBloc: _reservationBloc,
        sede: authenticationBloc.sede,
      );
    } else if (state is OnlineReservationForm) {
      return ReservarMatriculaPage(
        authenticationBloc: authenticationBloc,
        reservationBloc: _reservationBloc,
      );
    }
  }
}
