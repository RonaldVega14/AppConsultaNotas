import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class ReservationEvent extends Equatable {
  ReservationEvent([List props = const []]) : super(props);
}

class ReservationStarted extends ReservationEvent {
  @override
  String toString() => 'ReservationStarted';
}

class ToOnlineReservation extends ReservationEvent {
  @override
  String toString() => 'ToOnlineReservation';
}

class SendOnlineReservationForm extends ReservationEvent {
  //Datos de sede
  final String codCentro, codSede, nombreSede;

  //Datos personales
  final String nombre,
      apellido,
      sexo,
      fechaNac,
      dui,
      nie,
      idenGenero,
      nacionalidad,
      estadoFamiliar,
      medioTransporte,
      distanciaAlCentro,
      trabaja,
      ocupacion,
      poseeDiscapacida,
      discapacidad,
      retornado,
      paisRetorno;

  //Datos de residencia
  final String direccion,
      zonaResidencia,
      telResidencia,
      telCelular,
      telTrabajo,
      correoElectronica;

  //Datos sobre situacion familiar
  final String numMiembrosFamilia, convivencia, dependenciaEconomica, numHijos;

  final Map<String, String> nombreyEdadHijos;

  //Datos de estudios realizados
  final String ultimnoGradoCursado,
      anioDeUltimoCurso,
      tipoDeInstitucion,
      codInstitucion,
      nombreDelCentro;

  SendOnlineReservationForm(
      {@required this.codCentro,
      @required this.codSede,
      @required this.nombreSede,
      @required this.nombre,
      @required this.apellido,
      @required this.sexo,
      @required this.fechaNac,
      @required this.dui,
      @required this.nie,
      @required this.idenGenero,
      @required this.nacionalidad,
      @required this.estadoFamiliar,
      @required this.medioTransporte,
      this.distanciaAlCentro,
      @required this.trabaja,
      this.ocupacion = 'N/A',
      @required this.poseeDiscapacida,
      this.discapacidad = 'N/A',
      @required this.retornado,
      this.paisRetorno = 'N/A',
      @required this.direccion,
      @required this.zonaResidencia,
      @required this.telResidencia,
      @required this.telCelular,
      @required this.telTrabajo,
      this.correoElectronica,
      @required this.numMiembrosFamilia,
      @required this.convivencia,
      @required this.dependenciaEconomica,
      @required this.numHijos,
      @required this.nombreyEdadHijos,
      @required this.ultimnoGradoCursado,
      @required this.anioDeUltimoCurso,
      @required this.tipoDeInstitucion,
      @required this.codInstitucion,
      @required this.nombreDelCentro});
  @override
  String toString() => 'SendOnlineReservationForm';
}

class ToLoginPage extends ReservationEvent {
  @override
  String toString() => 'ToLoginPage';
}

class DownloadNewStudentForm extends ReservationEvent {
  final String url;
  final BuildContext context;

  DownloadNewStudentForm(this.url, this.context);
}
