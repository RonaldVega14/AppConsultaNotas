import 'package:app_margarita_duran/models/modulesModel.dart';
import 'package:app_margarita_duran/models/studentModel.dart';
import 'package:app_margarita_duran/models/subjectModel.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class HomeEvent extends Equatable {}

class ToHomePage extends HomeEvent {
  final Student student;

  ToHomePage({@required this.student});
  @override
  String toString() => 'ToHomePage';
}

class ToRecuparationSubjectsPage extends HomeEvent {
  final Module module;

  ToRecuparationSubjectsPage(this.module);
  @override
  String toString() => 'ToRecuparationSubjectsPage';
}

class DownloadRepositionForm extends HomeEvent {
  final String link;
  final bool recoverEnabled;

  DownloadRepositionForm({@required this.recoverEnabled, @required this.link});
  @override
  String toString() => 'DownloadRepositionForm';
}

class ToSubjectDetailsPage extends HomeEvent {
  final Subject subject;

  ToSubjectDetailsPage(this.subject);
  @override
  String toString() => 'ToSubjectDetailsPage';
}

class ToStudentProfilePage extends HomeEvent {
  final Student student;

  ToStudentProfilePage({@required this.student});
  @override
  String toString() => 'ToStudentProfilePage';
}

class InfoFormSubmit extends HomeEvent {
  final String phoneNumer, altPhoneNumber, email;
  final BuildContext context;
  bool fromProfilePage;

  InfoFormSubmit(
      {@required this.phoneNumer,
      @required this.altPhoneNumber,
      @required this.email,
      @required this.context,
      this.fromProfilePage = false});
  @override
  String toString() => 'InfoFormSubmit';
}

class FirstTimeInfoFormCancel extends HomeEvent {
  final BuildContext context;

  FirstTimeInfoFormCancel(this.context);
  @override
  String toString() => 'InfoFormCancel';
}
