import 'package:app_margarita_duran/events/reservationEvents.dart';
import 'package:app_margarita_duran/global/widgets.dart';
import 'package:app_margarita_duran/models/sedeModel.dart';
import 'package:app_margarita_duran/repositories/userRepository.dart';
import 'package:app_margarita_duran/states/reservationStates.dart';
import 'package:app_margarita_duran/utils/networkUtils.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class ReservationBloc extends Bloc<ReservationEvent, ReservationState> {
  final UserRepository userRepository;
  final Sede sede;

  ReservationBloc({@required this.userRepository, @required this.sede})
      : super(HomeReservation(sede: sede));

  ReservationState get initialState => HomeReservation(sede: sede);

  @override
  void onTransition(Transition<ReservationEvent, ReservationState> transition) {
    print(transition);
    super.onTransition(transition);
  }

  @override
  Stream<ReservationState> mapEventToState(
    ReservationEvent event,
  ) async* {
    if (event is ReservationStarted) {
      yield HomeReservation(sede: sede);
    } else if (event is ToOnlineReservation) {
      yield OnlineReservationForm();
    } else if (event is DownloadNewStudentForm) {
      GlobalWidgets.showInfoToast('Cargando formulario', null, null);
      if (await NetworkUtils.launchURL(event.url)) {
        GlobalWidgets.showInfoToast(
            'Formulario cargado exitosamente', null, null);
      } else {
        GlobalWidgets.showInfoToast(
            'Ocurrio un error al descargar el formulario', Colors.red, null);
      }
    } else {
      throw UnimplementedError();
    }
  }
}
