import 'package:app_margarita_duran/bloc/authenticationBloc.dart';
import 'package:app_margarita_duran/events/homeEvents.dart';
import 'package:app_margarita_duran/global/styles.dart';
import 'package:app_margarita_duran/global/widgets.dart';
import 'package:app_margarita_duran/repositories/userRepository.dart';
import 'package:app_margarita_duran/states/homeStates.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:validators/validators.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final AuthenticationBloc authenticationBloc;
  final UserRepository userRepository;
  SharedPreferences _prefs;
  HomeBloc(HomeState initialState,
      {@required this.authenticationBloc, @required this.userRepository})
      : super(initialState);

  @override
  void onTransition(Transition<HomeEvent, HomeState> transition) {
    print(transition);
    super.onTransition(transition);
  }

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is ToHomePage) {
      _prefs = await SharedPreferences.getInstance();
      if (event.student.user.firstTime) {
        if (_prefs.getBool('isFirst') != null) {
          if (_prefs.getBool('isFirst')) {
            yield FirstTimeInfoForm();
          }
        } else {
          yield FirstTimeInfoForm();
        }
      }
      yield OnHomePage(event.student);
    } else if (event is ToRecuparationSubjectsPage) {
      yield OnRecuperationPage(event.module);
    } else if (event is DownloadRepositionForm) {
      print(event.link + event.recoverEnabled.toString());
      if (event.recoverEnabled) {
        if (event.link != null && event.link != 'null') {
          if (isURL(event.link)) {
            Fluttertoast.showToast(
                msg: 'Descargando trabajo de recuperación',
                backgroundColor: MyColors.mainColor);
            //TODO: Descargar link de recuperacion
          } else {
            Fluttertoast.showToast(
                msg: 'Link de reposición invalido',
                toastLength: Toast.LENGTH_LONG,
                backgroundColor: Colors.red);
          }
        } else {
          Fluttertoast.showToast(
              msg: 'El trabajo de recuperación ya no se encuentra disponible',
              toastLength: Toast.LENGTH_LONG,
              backgroundColor: Colors.red);
        }
      } else {
        Fluttertoast.showToast(
            msg: 'El trabajo de recuperación ya no se encuentra disponible',
            toastLength: Toast.LENGTH_LONG,
            backgroundColor: Colors.red);
      }
    } else if (event is ToStudentProfilePage) {
      yield OnStudentProfilePage(event.student);
    } else if (event is InfoFormSubmit) {
      if (event.phoneNumer != null && event.phoneNumer.isNotEmpty) {
        if (event.altPhoneNumber != null && event.altPhoneNumber.isNotEmpty) {
          if (event.email != null && event.email.isNotEmpty) {
            final response = await userRepository.updateStudentContactInfo(
                token: authenticationBloc.student.user.token,
                phoneNumber: event.phoneNumer,
                email: event.email,
                altPhoneNumber: event.altPhoneNumber);
            if (response) {
              authenticationBloc.student.phoneNumber = event.phoneNumer;
              authenticationBloc.student.email = event.email;
              authenticationBloc.student.altPhoneNumber = event.altPhoneNumber;
              if (event.fromProfilePage) {
                Fluttertoast.showToast(
                    msg: 'Perfil actualizado con exito',
                    backgroundColor: MyColors.mainColor);
                yield OnStudentProfilePage(authenticationBloc.student);
              } else {
                GlobalWidgets.hideProgress(event.context);
                Fluttertoast.showToast(
                    msg: 'Perfil actualizado con exito',
                    backgroundColor: MyColors.mainColor);
                yield OnHomePage(authenticationBloc.student);
              }
            } else {
              Fluttertoast.showToast(
                  msg: 'Error de conexion, intentelo de nuevo',
                  backgroundColor: Colors.red);
              yield OnHomePage(authenticationBloc.student);
            }
          } else {
            Fluttertoast.showToast(
                msg: 'Por favor ingresar su correo electronico',
                backgroundColor: Colors.red);
            yield OnHomePage(authenticationBloc.student);
          }
        } else {
          Fluttertoast.showToast(
              msg:
                  'Por favor ingresar un numero de contacto en caso de emergencia',
              backgroundColor: Colors.red);
          yield OnHomePage(authenticationBloc.student);
        }
      } else {
        Fluttertoast.showToast(
            msg: 'Por favor ingresar su numero de telefono',
            backgroundColor: Colors.red);
        yield OnHomePage(authenticationBloc.student);
      }
    } else if (event is FirstTimeInfoFormCancel) {
      _prefs = await SharedPreferences.getInstance();
      _prefs.setBool('isFirst', false);
      GlobalWidgets.hideProgress(event.context);
    } else {
      throw UnimplementedError();
    }
  }
}
