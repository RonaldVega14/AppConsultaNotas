import 'package:app_margarita_duran/events/authenticationEvents.dart';
import 'package:app_margarita_duran/global/styles.dart';
import 'package:app_margarita_duran/global/widgets.dart';
import 'package:app_margarita_duran/models/sedeModel.dart';
import 'package:app_margarita_duran/models/studentModel.dart';
import 'package:app_margarita_duran/models/userModel.dart';
import 'package:app_margarita_duran/repositories/sedeRepository.dart';
import 'package:app_margarita_duran/repositories/userRepository.dart';
import 'package:app_margarita_duran/states/authenticationStates.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:meta/meta.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository userRepository;

  AuthenticationBloc({@required this.userRepository})
      : super(AuthenticationUninitialized());

  AuthenticationState get initialState => AuthenticationUninitialized();

  User user = User();
  Student student = Student(user: User());
  Sede _sede = Sede();
  Sede get sede => _sede;
  bool sedeValid = false;
  int totalModules;
  SedeRepository sedeRepository = SedeRepository();

  @override
  void onTransition(
      Transition<AuthenticationEvent, AuthenticationState> transition) {
    print(transition);
    super.onTransition(transition);
  }

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      final sedeResponse = await sedeRepository.getSedeInfo(token: null);
      if (sedeResponse != null) {
        _sede = sedeResponse;
        sedeValid = true;
        yield AuthenticationUnauthenticated(sede: sedeResponse);
      } else {
        yield AuthenticationUnauthenticated(
            sede: Sede(
                address:
                    "Calle daniel hernandez y 4ta av. Sur sta. Tecla, SANTA TECLA, LA LIBERTAD.",
                code: 'E0108',
                logo: 'assets/images/badge.png',
                id: '1',
                name: 'Margarita Duran'));
      }
    }

    if (event is LoggedIn) {
      yield AuthenticationLoading();
      //Verificando si es primera vez que entra.
      final User response =
          await userRepository.authenticate(username: event.username);
      if (response != null && response.token != null) {
        user = response;
        user.code = event.username;

        final studentInfo =
            await userRepository.getStudentInfo(token: user.token);
        if (studentInfo != null) {
          student = studentInfo;
          student.user = user;
          totalModules =
              await userRepository.getModulesQuanatity(token: user.token);
          Fluttertoast.showToast(
              msg: 'Bienvenido/a ${student.firstName}',
              backgroundColor: MyColors.mainColor);

          yield AuthenticationAuthenticated();
        } else {
          Fluttertoast.showToast(
              msg: 'Ocurrio un error al obtener las notas',
              backgroundColor: Colors.red);
          yield AuthenticationUnauthenticated(
              sede: sedeValid
                  ? _sede
                  : Sede(
                      address:
                          "Calle daniel hernandez y 4ta av. Sur sta. Tecla, SANTA TECLA, LA LIBERTAD.",
                      code: 'E0108',
                      logo: 'assets/images/badge.png',
                      id: '1',
                      name: 'Margarita Duran'));
        }
      } else {
        Fluttertoast.showToast(
            msg: 'Error de autenticacion, intentelo de nuevo.',
            backgroundColor: Colors.red);
        yield AuthenticationUnauthenticated(
            sede: sedeValid
                ? _sede
                : Sede(
                    address:
                        "Calle daniel hernandez y 4ta av. Sur sta. Tecla, SANTA TECLA, LA LIBERTAD.",
                    code: 'E0108',
                    logo: 'assets/images/badge.png',
                    id: '1',
                    name: 'Margarita Duran'));
      }
    }

    if (event is InfoFormCancel) {
      GlobalWidgets.hideProgress(event.context);
      yield AuthenticationAuthenticated();
    }

    if (event is LoggedOut) {
      yield AuthenticationLoading();
      user = User();
      student = Student(user: User());
      yield AuthenticationUnauthenticated(
          sede: sedeValid
              ? _sede
              : Sede(
                  address:
                      "Calle daniel hernandez y 4ta av. Sur sta. Tecla, SANTA TECLA, LA LIBERTAD.",
                  code: 'E0108',
                  logo: 'assets/images/badge.png',
                  id: '1',
                  name: 'Margarita Duran'));
    }

    if (event is ToNuevaMatriculaOptions) {
      yield NuevaMatriculaOptions();
    }

    if (event is BackToLogin) {
      yield AuthenticationUnauthenticated(
          sede: sedeValid
              ? _sede
              : Sede(
                  address:
                      "Calle daniel hernandez y 4ta av. Sur sta. Tecla, SANTA TECLA, LA LIBERTAD.",
                  code: 'E0108',
                  logo: 'assets/images/badge.png',
                  id: '1',
                  name: 'Margarita Duran'));
    }

    if (event is BackNuevaMatriculaOptions) {
      yield NuevaMatriculaOptions();
    }
  }
}
