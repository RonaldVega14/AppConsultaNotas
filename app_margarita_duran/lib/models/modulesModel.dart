import 'package:app_margarita_duran/models/subjectModel.dart';

class Module {
  String id;
  List<Subject> subjects;

  Module({this.id, this.subjects});

  static List<Module> fromJsonObject(Map<String, dynamic> json) {
    List<Module> allModules = [];
    List<Subject> tempSubjects = [];
    Subject temp;

    if (json != null) {
      for (var i = 1; i < json.length; i++) {
        if (json[i.toString()] != null) {
          Module tempModule = Module();
          tempModule.subjects = [];
          tempSubjects.clear();
          List<dynamic> moduleSubjects = json[i.toString()];
          for (var i = 0; i < moduleSubjects.length; i++) {
            temp = Subject.fromJson(moduleSubjects[i]);
            if (temp != null) {
              tempModule.subjects.add(temp);
            }
          }
          tempModule.id = i.toString();
          allModules.add(tempModule);
        }
      }

      if (json['externalTest'] != null) {
        Module tempModule = Module();
        tempModule.subjects = [];
        tempSubjects.clear();
        List<dynamic> moduleSubjects = json['externalTest'];
        for (var i = 0; i < moduleSubjects.length; i++) {
          temp = Subject.fromJson(moduleSubjects[i]);
          if (temp != null) {
            tempModule.subjects.add(temp);
          }
        }
        tempModule.id = 'externalTest';
        allModules.add(tempModule);
      }
    }
    return allModules;
  }
}
