class Sede {
  String id;
  String name;
  String logo;
  String code;
  String address;

  Sede({this.address, this.code, this.id, this.logo, this.name});

  factory Sede.fromJson(Map<String, dynamic> json) {
    return Sede(
        id: json['id'].toString() ?? '',
        name: json['name'].toString() ?? '',
        logo: json['logo'].toString() ?? '',
        code: json['code'].toString() ?? '',
        address: json['address'].toString() ?? '');
  }
}
