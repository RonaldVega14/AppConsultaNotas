import 'package:app_margarita_duran/utils/generalFunctions.dart';

class Subject {
  String subject;
  String id;
  String note;
  bool approved;
  String recoverLink;
  bool recoverEnabled;
  bool isExternalTest;
  String createdAt;
  String updatedAt;
  String module;

  Subject(
      {this.approved,
      this.module,
      this.id,
      this.isExternalTest,
      this.note,
      this.recoverEnabled,
      this.recoverLink,
      this.subject,
      this.updatedAt,
      this.createdAt});

  factory Subject.fromJson(Map<String, dynamic> json) {
    return Subject(
        approved: json['approved'] ?? false,
        module: json['module'].toString() ?? '',
        id: json['id'].toString() ?? '',
        isExternalTest: json['isExternalTest'] ?? false,
        note: json['note'].toString(),
        recoverEnabled: json['recoverEnabled'] ?? false,
        recoverLink: json['recoverLink'].toString() ?? '',
        subject: json['subject'].toString() ?? '',
        updatedAt: convertDateFromString(json['updatedAt'].toString()),
        createdAt: convertDateFromString(json['createdAt'].toString()));
  }
}
