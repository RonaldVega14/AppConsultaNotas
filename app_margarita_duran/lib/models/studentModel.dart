import 'package:app_margarita_duran/models/modulesModel.dart';
import 'package:app_margarita_duran/models/userModel.dart';
import 'package:flutter/material.dart';

class Student {
  User user;
  String id;
  String year;
  String report;
  bool approved;
  double finalAverage;
  double institutionalAverage;
  bool firstTime;
  String modality;
  String section;
  String grade;
  String username;
  String firstName;
  String lastName;
  String phoneNumber;
  String email;
  String altPhoneNumber;
  bool status;
  List<Module> modules;

  Student(
      {this.altPhoneNumber,
      this.approved,
      this.email,
      this.finalAverage,
      this.firstName,
      this.firstTime,
      this.grade,
      this.id,
      this.institutionalAverage,
      this.lastName,
      this.modality,
      this.modules = const [],
      this.phoneNumber,
      this.report,
      this.section,
      this.status,
      @required this.user,
      this.username,
      this.year});

  factory Student.fromJson(Map<String, dynamic> json) {
    return Student(
      user: User(),
      approved: json['approved'] ?? false,
      altPhoneNumber: json['altPhoneNumber'].toString(),
      email: json['email'].toString() ?? '',
      firstName: json['firstName'].toString() ?? '',
      grade: json['grade'].toString() ?? '',
      id: json['id'].toString() ?? '',
      lastName: json['lastName'].toString() ?? '',
      modality: json['modality'].toString() ?? '',
      phoneNumber: json['phoneNumber'].toString() ?? '',
      report: json['report'].toString() ?? '',
      section: json['section'].toString() ?? '',
      username: json['username'].toString() ?? '',
      year: json['year'].toString() ?? '',
      finalAverage: json['finalAverage'] ?? 0.0,
      firstTime: json['firstTime'] ?? true,
      institutionalAverage: json['institutionalAverage'] ?? 0.0,
      status: json['status'] ?? false,
      modules:
          json['modules'] != null ? Module.fromJsonObject(json['modules']) : [],
    );
  }
}
