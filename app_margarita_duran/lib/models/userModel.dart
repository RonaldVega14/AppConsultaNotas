class User {
  String code;
  final String token;
  final bool firstTime;

  User({this.code, this.token, this.firstTime});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        token: json['token'].toString() ?? '',
        firstTime: json['firstTime'] ?? true);
  }
}
