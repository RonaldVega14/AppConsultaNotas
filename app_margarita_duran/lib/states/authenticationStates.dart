import 'package:app_margarita_duran/models/sedeModel.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AuthenticationState extends Equatable {}

class AuthenticationUninitialized extends AuthenticationState {
  @override
  String toString() => 'AuthenticationUninitialized';
}

class AuthenticationAuthenticated extends AuthenticationState {
  @override
  String toString() => 'AuthenticationAuthenticated';
}

class AuthenticationUnauthenticated extends AuthenticationState {
  final Sede sede;

  AuthenticationUnauthenticated({@required this.sede});

  @override
  String toString() => 'AuthenticationUnauthenticated';
}

class AuthenticationLoading extends AuthenticationState {
  @override
  String toString() => 'AuthenticationLoading';
}

class NuevaMatriculaOptions extends AuthenticationState {
  @override
  String toString() => 'NuevaMatriculaOptions';
}

class NuevaMatriculaForm extends AuthenticationState {
  @override
  String toString() => 'NuevaMatriculaForm';
}
