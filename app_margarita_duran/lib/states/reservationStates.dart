import 'package:app_margarita_duran/models/sedeModel.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ReservationState extends Equatable {}

class HomeReservation extends ReservationState {
  final Sede sede;

  HomeReservation({@required this.sede});
  @override
  String toString() => 'HomeReservation';
}

class OnlineReservationForm extends ReservationState {
  @override
  String toString() => 'OnlineReservationForm';
}

class BackToLogin extends ReservationState {
  @override
  String toString() => 'BackToLogin';
}
