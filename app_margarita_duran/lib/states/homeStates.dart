import 'package:app_margarita_duran/models/modulesModel.dart';
import 'package:app_margarita_duran/models/studentModel.dart';
import 'package:app_margarita_duran/models/subjectModel.dart';
import 'package:equatable/equatable.dart';

abstract class HomeState extends Equatable {}

class OnHomePage extends HomeState {
  final Student student;

  OnHomePage(this.student);
  @override
  String toString() => 'OnHomePage';
}

class OnRecuperationPage extends HomeState {
  final Module module;

  OnRecuperationPage(this.module);
  @override
  String toString() => 'OnRecuperationPage';
}

class OnSubjectDetailsPage extends HomeState {
  final Subject subject;

  OnSubjectDetailsPage(this.subject);
  @override
  String toString() => 'OnSubjectDetailsPage';
}

class OnStudentProfilePage extends HomeState {
  final Student student;

  OnStudentProfilePage(this.student);
  @override
  String toString() => 'OnStudentProfilePage';
}

class FirstTimeInfoForm extends HomeState {
  @override
  String toString() => 'FirstTimeInfoForm';
}
