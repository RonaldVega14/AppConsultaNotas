import 'package:url_launcher/url_launcher.dart';

class NetworkUtils {
  static final String path = "https://educame-sede-api.herokuapp.com/api/";
  static final String defaultSignUpFormLink =
      'https://onedrive.live.com/?authkey=%21AItSLaRfVVRvB-A&cid=49559FE212844F55&id=49559FE212844F55%213654&parId=49559FE212844F55%213652&o=OneUp';

  static Future<bool> launchURL(String url) async {
    if (await canLaunch(url)) {
      return await launch(url);
    } else {
      print('Could not launch $url');
      return false;
    }
  }
}
