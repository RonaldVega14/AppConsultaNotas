import 'dart:convert';

import 'package:app_margarita_duran/models/studentModel.dart';
import 'package:app_margarita_duran/models/userModel.dart';
import 'package:app_margarita_duran/utils/networkUtils.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

String tag = 'UserRepository';

class UserRepository {
  User user;
  Student student;

  //Login method
  Future<User> authenticate({
    @required String username,
  }) async {
    String url = NetworkUtils.path + 'auth/login';
    try {
      final response = await http.post(url,
          body: json.encode({"code": username}),
          headers: {"Content-Type": "application/json"});
      if (response != null && response.statusCode == 200) {
        user = User.fromJson(json.decode(response.body));
        return user;
      } else {
        print('ERROR in $tag: authenticate: response: ' +
            response.body.toString());
        return null;
      }
    } catch (e) {
      print('ERROR in $tag: authenticate: ${e.toString()}');
    }
    return null;
  }

  Future<Student> getStudentInfo({@required String token}) async {
    String url = NetworkUtils.path + 'student/notes';
    try {
      final response = await http.get(
        url,
        headers: {
          'Authorization': 'Bearer $token',
        },
      );
      if (response != null && response.statusCode == 200) {
        student = Student.fromJson(json.decode(response.body));
        student.user = user;
        return student;
      }
    } catch (e) {
      print('ERROR in $tag: getStudentInfo: ${e.toString()}');
    }
    return null;
  }

  Future<bool> updateStudentContactInfo(
      {@required String token,
      @required String phoneNumber,
      @required String email,
      @required String altPhoneNumber}) async {
    String url = NetworkUtils.path + 'student/contact';
    try {
      final response = await http.post(
        url,
        body: json.encode({
          "phoneNumber": phoneNumber,
          "email": email,
          "altPhoneNumber": altPhoneNumber
        }),
        headers: {
          'Authorization': 'Bearer $token',
          'Content-Type': 'application/json'
        },
      );
      if (response != null && response.statusCode == 200) {
        return true;
      }
    } catch (e) {
      print('ERROR in $tag: updateStudentContactInfo: ${e.toString()}');
    }
    return false;
  }

  Future<int> getModulesQuanatity({@required String token}) async {
    String url = NetworkUtils.path + 'modules';
    try {
      final response = await http.get(
        url,
        headers: {
          'Authorization': 'Bearer $token',
        },
      );
      if (response != null && response.statusCode == 200) {
        return _getNumberOfElements(json.decode(response.body));
      }
    } catch (e) {
      print('ERROR in $tag: getModulesQuanatity: ${e.toString()}');
    }
    return 0;
  }

  int _getNumberOfElements(List<dynamic> json) {
    if (json != null) {
      return json.length;
    }
    return 0;
  }
}
