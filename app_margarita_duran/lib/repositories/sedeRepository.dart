import 'dart:convert';

import 'package:app_margarita_duran/models/sedeModel.dart';
import 'package:app_margarita_duran/utils/networkUtils.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

String tag = 'SedeRepository';

class SedeRepository {
  Sede sede;

  Future<Sede> getSedeInfo({@required String token}) async {
    final url = NetworkUtils.path + 'sede-information';
    try {
      final response = await http.get(
        url,
        headers: {
          'Authorization': 'Bearer $token',
        },
      );

      if (response.statusCode == 200) {
        sede = Sede.fromJson(json.decode(response.body));
        return sede;
      }
    } catch (e) {
      print('ERROR in $tag: getStudentInfo: ${e.toString()}');
    }
    return null;
  }
}
