import 'package:app_margarita_duran/bloc/authenticationBloc.dart';
import 'package:app_margarita_duran/bloc/homeBloc.dart';
import 'package:app_margarita_duran/global/styles.dart';
import 'package:app_margarita_duran/global/widgets.dart';
import 'package:app_margarita_duran/models/modulesModel.dart';
import 'package:app_margarita_duran/models/studentModel.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final AuthenticationBloc authenticationBloc;
  final HomeBloc homeBloc;
  final Student student;

  const HomePage(
      {Key key,
      @required this.authenticationBloc,
      @required this.homeBloc,
      @required this.student})
      : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  AuthenticationBloc get _authenticationBloc => widget.authenticationBloc;
  HomeBloc get _homeBloc => widget.homeBloc;
  GlobalKey _scaffoldKey = new GlobalKey();
  TabController _tabController;
  Student get student => widget.student;

  @override
  void initState() {
    _tabController =
        new TabController(length: student.modules.length + 1, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        drawer: GlobalWidgets.customDrawer(
            context, _authenticationBloc, _homeBloc, student),
        backgroundColor: Colors.white,
        // Appbar
        appBar: GlobalWidgets.appBar(context, _authenticationBloc),
        resizeToAvoidBottomPadding: true,
        // Body
        body: SafeArea(child: buildScreen(context)));
  }

  buildScreen(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: ListView(children: [
        _buildInfoText(),
        Container(
          margin: EdgeInsets.only(top: 5),
          width: MediaQuery.of(context).size.width,
          color: Color.fromRGBO(64, 186, 213, .3),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                'INFORME DE RESULTADOS ${student.year}',
                style: TextStyle(
                    fontFamily: Fonts.secondaryFont,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        // Tabs Header
        TabBar(
            controller: _tabController,
            labelStyle: TextStyle(
                fontFamily: Fonts.primaryFont,
                fontSize: 17,
                fontWeight: FontWeight.bold),
            labelColor: MyColors.mainColor,
            unselectedLabelColor: Colors.black38,
            isScrollable: true,
            tabs: _buildTabLabels(student.modules)),
        Container(
          constraints: BoxConstraints(maxHeight: 850, minWidth: 700),
          child: TabBarView(
              controller: _tabController,
              children: _buildTabContents(student.modules)),
        ),
      ]),
    );
  }

  List<Widget> _buildTabLabels(List<Module> modules) {
    List<Tab> allTabs = [];
    allTabs = modules
        .map(
          (e) => e.id == 'externalTest'
              ? Tab(text: 'Nota final'.toUpperCase())
              : Tab(
                  text: 'Modulo ${e.id}'.toUpperCase(),
                ),
        )
        .toList();
    allTabs.add(Tab(text: 'Promedio final'.toUpperCase()));
    return allTabs;
  }

  List<Widget> _buildTabContents(List<Module> modules) {
    List<Widget> moduleWidget = [];
    moduleWidget = modules.map((module) => createTab(module)).toList();
    moduleWidget.add(createFinalTab());
    return moduleWidget;
  }

  List<Widget> _buildFinalTab() {
    List<Widget> subjectsComponents = [];

    subjectsComponents.add(GlobalWidgets.createAvgWidget(
        context: context,
        materia: 'Promedio Institucional',
        nota: student.institutionalAverage.toString() == 'null'
            ? 'No publicado'
            : student.institutionalAverage.toString() == '0.0'
                ? 'No publicado'
                : student.institutionalAverage.toString(),
        aprobado: student.institutionalAverage > 6.0));
    subjectsComponents.add(GlobalWidgets.createAvgWidget(
        context: context,
        materia: 'Promedio Final',
        nota: student.finalAverage.toString() == 'null'
            ? 'No publicado'
            : student.finalAverage.toString() == '0.0'
                ? 'No publicado'
                : student.finalAverage.toString(),
        aprobado: student.finalAverage > 6.0));

    subjectsComponents.add(SizedBox(
      height: 20,
    ));
    return subjectsComponents;
  }

  List<Widget> _buildTabInfo(Module module) {
    List<Widget> subjectsComponents = [];
    int subjectsFailed = 0;
    if (module.id != 'externalTest') {
      for (var subject in module.subjects) {
        if (!subject.approved) {
          subjectsFailed++;
        }
        subjectsComponents.add(GlobalWidgets.createAsignatura(
            context: context,
            materia: subject.subject,
            nota: subject.note,
            aprobado: subject.approved));
      }
      subjectsComponents.add(SizedBox(
        height: 20,
      ));
      subjectsFailed == 0
          ? subjectsComponents.add(
              GlobalWidgets.reposicionBtn(
                  'No tienes materias por reponer', context, null, null,
                  isDisable: true),
            )
          : subjectsComponents.add(GlobalWidgets.reposicionBtn(
              'Obtener pruebas de reposición |  ${subjectsFailed.toString()}',
              context,
              module,
              _homeBloc,
            ));
      return subjectsComponents;
    } else {
      for (var subject in module.subjects) {
        if (!subject.approved) {
          subjectsFailed++;
        }
        subjectsComponents.add(GlobalWidgets.createAsignatura(
            context: context,
            materia: subject.subject,
            nota: subject.note,
            aprobado: subject.approved));
      }
      subjectsComponents.add(SizedBox(
        height: 20,
      ));
      subjectsFailed == 0
          ? subjectsComponents.add(
              GlobalWidgets.reposicionBtn('Aprobado', context, null, null,
                  isDisable: true),
            )
          : subjectsComponents.add(GlobalWidgets.reposicionBtn(
              'Reprobado',
              context,
              module,
              _homeBloc,
            ));
    }

    return subjectsComponents;
  }

  _buildInfoText() {
    return Column(children: [
      // Badge
      Center(
        child: Container(
          height: 110,
          width: 110,
          child: Image.network(
            _authenticationBloc.sede.logo,
            errorBuilder: (context, error, stackTrace) =>
                Image.asset('assets/images/badge.png'),
          ),
        ),
      ),
      SizedBox(height: 8),
      // Info Text
      Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                // Nombre del estudiante
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Estudiante: ',
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                        ),
                      ),
                      Text(
                        student.firstName + ' ' + student.lastName,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                        ),
                      )
                    ],
                  ),
                ),
                // Modalidad
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Modalidad: ',
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                        ),
                      ),
                      Text(
                        student.modality,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                        ),
                      )
                    ],
                  ),
                ),
                // Año
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Grado: ',
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                        ),
                      ),
                      Text(
                        '${student.grade}',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                        ),
                      )
                    ],
                  ),
                ),
                // Año y sección
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      // Año
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Año: ',
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: Fonts.primaryFont,
                            ),
                          ),
                          Text(
                            student.year,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              fontFamily: Fonts.primaryFont,
                            ),
                          )
                        ],
                      ),
                      // Sección
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Sección: ',
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: Fonts.primaryFont,
                            ),
                          ),
                          Text(
                            ' "${student.section}" ',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              fontFamily: Fonts.primaryFont,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      )
    ]);
  }

  Widget createTab(Module module) {
    return Center(child: Column(children: _buildTabInfo(module)));
  }

  Widget createFinalTab() {
    return Center(child: Column(children: _buildFinalTab()));
  }
}
