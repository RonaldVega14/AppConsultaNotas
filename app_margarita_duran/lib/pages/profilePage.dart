import 'package:app_margarita_duran/bloc/authenticationBloc.dart';
import 'package:app_margarita_duran/bloc/homeBloc.dart';
import 'package:app_margarita_duran/events/homeEvents.dart';
import 'package:app_margarita_duran/global/styles.dart';
import 'package:app_margarita_duran/global/widgets.dart';
import 'package:app_margarita_duran/models/studentModel.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  final Student student;
  final AuthenticationBloc authenticationBloc;
  final HomeBloc homeBloc;

  const ProfilePage(
      {Key key,
      @required this.student,
      @required this.authenticationBloc,
      @required this.homeBloc})
      : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  Student get student => widget.student;
  AuthenticationBloc get authenticationBloc => widget.authenticationBloc;
  HomeBloc get homeBloc => widget.homeBloc;

  TextEditingController phoneController = TextEditingController();
  TextEditingController altPhoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  @override
  void initState() {
    if (student.phoneNumber != null &&
        student.phoneNumber.isNotEmpty &&
        student.phoneNumber != 'null') {
      phoneController.text = student.phoneNumber;
    }
    if (student.altPhoneNumber != null &&
        student.altPhoneNumber.isNotEmpty &&
        student.altPhoneNumber != 'null') {
      altPhoneController.text = student.altPhoneNumber;
    }
    if (student.email != null &&
        student.email.isNotEmpty &&
        student.email != 'null') {
      emailController.text = student.email;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: GlobalWidgets.customDrawer(
          context, widget.authenticationBloc, widget.homeBloc, widget.student),
      backgroundColor: Colors.white,
      // Appbar
      appBar: GlobalWidgets.appBar(context, widget.authenticationBloc),
      resizeToAvoidBottomPadding: true,
      body: ListView(
        children: [
          _buildInfoText(context),
          Container(
            margin: EdgeInsets.only(top: 5),
            width: MediaQuery.of(context).size.width,
            color: Color.fromRGBO(64, 186, 213, .3),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  'Informacion de contacto'.toUpperCase(),
                  style: TextStyle(
                      fontFamily: Fonts.secondaryFont,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          _buildContactForm(context),
        ],
      ),
    );
  }

  _buildInfoText(BuildContext context) {
    return Column(children: [
      // Badge
      Center(
        child: Container(
          height: 110,
          width: 110,
          child: Image.network(
            widget.authenticationBloc.sede.logo,
            errorBuilder: (context, error, stackTrace) =>
                Image.asset('assets/images/badge.png'),
          ),
        ),
      ),
      SizedBox(height: 8),
      // Info Text
      Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                // Nombre del estudiante
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Estudiante: ',
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                        ),
                      ),
                      Text(
                        widget.student.firstName +
                            ' ' +
                            widget.student.lastName,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                        ),
                      )
                    ],
                  ),
                ),
                // Modalidad
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Modalidad: ',
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                        ),
                      ),
                      Text(
                        widget.student.modality,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                        ),
                      )
                    ],
                  ),
                ),
                // Año
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Grado: ',
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                        ),
                      ),
                      Text(
                        '${widget.student.grade}',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                        ),
                      )
                    ],
                  ),
                ),
                // Año y sección
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      // Año
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Año: ',
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: Fonts.primaryFont,
                            ),
                          ),
                          Text(
                            widget.student.year,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              fontFamily: Fonts.primaryFont,
                            ),
                          )
                        ],
                      ),
                      // Sección
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Sección: ',
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: Fonts.primaryFont,
                            ),
                          ),
                          Text(
                            ' "${widget.student.section}" ',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              fontFamily: Fonts.primaryFont,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      )
    ]);
  }

  _buildContactForm(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        child: Column(
          children: [
            Row(
              children: [
                Flexible(
                  fit: FlexFit.loose,
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Icon(Icons.account_balance,
                        size: 28, color: Color.fromRGBO(3, 90, 166, 1)),
                  ),
                ),
                Flexible(
                  fit: FlexFit.loose,
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Text(
                      'DATOS DE SEDE',
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
            // Campos para ingresar informacion de contacto
            SizedBox(
              height: 5,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(143, 148, 251, .2),
                        blurRadius: 20.0,
                        offset: Offset(0, 10))
                  ]),
              child: Column(
                children: [
                  // telefono del estudiante
                  GlobalWidgets.customFormField(phoneController,
                      labeltxt: 'Telefono de contacto',
                      leadingIcon: Icons.dialpad,
                      hintTxt: ''),
                  // Correo electronico
                  GlobalWidgets.customFormField(emailController,
                      labeltxt: 'Correo electrónico',
                      leadingIcon: Icons.email_rounded,
                      hintTxt: ''),
                  // Telefono de emergencia
                  GlobalWidgets.customFormField(altPhoneController,
                      labeltxt: 'Telefono de emergencia',
                      leadingIcon: Icons.dialpad,
                      hintTxt: ''),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            GlobalWidgets.mainBtn(
                'Enviar',
                context,
                homeBloc,
                InfoFormSubmit(
                    altPhoneNumber: altPhoneController.text,
                    email: emailController.text,
                    phoneNumer: phoneController.text,
                    context: context,
                    fromProfilePage: true)),
          ],
        ),
      );
}
