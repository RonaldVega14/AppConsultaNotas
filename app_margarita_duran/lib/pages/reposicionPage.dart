import 'package:app_margarita_duran/bloc/authenticationBloc.dart';
import 'package:app_margarita_duran/bloc/homeBloc.dart';
import 'package:app_margarita_duran/global/styles.dart';
import 'package:app_margarita_duran/global/widgets.dart';
import 'package:app_margarita_duran/models/modulesModel.dart';
import 'package:app_margarita_duran/models/subjectModel.dart';
import 'package:flutter/material.dart';

class ReposicionPage extends StatelessWidget {
  final AuthenticationBloc authenticationBloc;
  final HomeBloc homeBloc;
  final Module module;

  const ReposicionPage(
      {Key key,
      @required this.authenticationBloc,
      @required this.homeBloc,
      @required this.module})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: GlobalWidgets.customDrawer(
            context, authenticationBloc, homeBloc, authenticationBloc.student),
        backgroundColor: Colors.white,
        // Appbar
        appBar: GlobalWidgets.appBar(context, authenticationBloc),
        resizeToAvoidBottomPadding: true,
        // Body
        body: SafeArea(
            child: ListView(children: [
          // Badge
          Center(
            child: Container(
              height: 110,
              width: 110,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.contain,
                      image: AssetImage('assets/images/badge.png'))),
            ),
          ),
          SizedBox(height: 8),
          // Info Text
          Container(
            margin: EdgeInsets.only(top: 5),
            width: MediaQuery.of(context).size.width,
            color: Color.fromRGBO(64, 186, 213, .3),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  'RECUPERACIÓN MODULO ${module.id}',
                  style: TextStyle(
                      fontFamily: Fonts.secondaryFont,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          ListView.builder(
              shrinkWrap: true,
              itemCount: module.subjects.length,
              itemBuilder: (BuildContext context, int index) =>
                  _isApproved(module.subjects[index])
                      ? SizedBox()
                      : GlobalWidgets.createReposicionWidget(
                          subject: module.subjects[index],
                          homeBloc: homeBloc,
                          context: context))
        ])));
  }

  bool _isApproved(Subject subject) {
    if (subject.approved) {
      return true;
    }
    return false;
  }
}
