import 'package:app_margarita_duran/bloc/authenticationBloc.dart';
import 'package:app_margarita_duran/bloc/reservationBloc.dart';
import 'package:app_margarita_duran/events/authenticationEvents.dart';
import 'package:app_margarita_duran/events/reservationEvents.dart';
import 'package:app_margarita_duran/global/styles.dart';
import 'package:app_margarita_duran/models/sedeModel.dart';
import 'package:app_margarita_duran/utils/networkUtils.dart';
import 'package:flutter/material.dart';

class NuevoIngresoPage extends StatelessWidget {
  final AuthenticationBloc authenticationBloc;
  final ReservationBloc reservationBloc;
  final Sede sede;

  const NuevoIngresoPage(
      {Key key,
      @required this.authenticationBloc,
      @required this.reservationBloc,
      @required this.sede})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Color.fromRGBO(255, 255, 255, 1),
          elevation: 0,
          actions: [
            IconButton(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                iconSize: 28,
                tooltip: 'Retroceder',
                icon: Icon(Icons.arrow_back),
                color: Color.fromRGBO(3, 90, 166, 1),
                onPressed: () {
                  authenticationBloc.add(BackToLogin());
                }),
          ]),
      resizeToAvoidBottomPadding: true,
      body: SafeArea(
          child: Stack(
        children: [
          // Background image
          Positioned(
            left: 0,
            bottom: 0,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * .7,
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fitWidth,
                      image: AssetImage('assets/images/bg2.png'))),
            ),
          ),
          Positioned(
            child: ListView(
              children: [
                // Titulo y logo
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      flex: 3,
                      fit: FlexFit.loose,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 28.0),
                        child: Container(
                          height: 80,
                          width: 85,
                          child: Image.network(
                            sede.logo,
                            errorBuilder: (context, error, stackTrace) =>
                                Image.asset('assets/images/badge.png'),
                          ),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 6,
                      fit: FlexFit.tight,
                      child: Container(
                        padding: EdgeInsets.only(bottom: 25),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          'NUEVO INGRESO',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontFamily: Fonts.secondaryFont,
                              fontSize: 24,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
                // Opcion de reservar matricula online
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                  height: 80,
                  width: MediaQuery.of(context).size.width,
                  child: GestureDetector(
                    onTap: () {
                      reservationBloc.add(ToOnlineReservation());
                    },
                    child: Material(
                      color: Colors.white.withOpacity(0.9),
                      elevation: 5.0,
                      shadowColor: Colors.black,
                      borderRadius: BorderRadius.circular(14.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Flexible(
                            flex: 3,
                            fit: FlexFit.loose,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: CircleAvatar(
                                minRadius: 28,
                                maxRadius: 30,
                                backgroundColor:
                                    Color.fromRGBO(3, 90, 166, 0.8),
                                child: Icon(
                                  Icons.mail_outline,
                                  color: Colors.white,
                                  size: 28,
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 7,
                            fit: FlexFit.tight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Center(
                                child: Text(
                                  'Reservar matricula online'.toUpperCase(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: Fonts.primaryFont,
                                      fontWeight: FontWeight.w600,
                                      color: Color.fromRGBO(3, 90, 166, 1),
                                      fontSize: 20),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                // Opcion de descargar matricula online
                Container(
                  margin: EdgeInsets.only(left: 30, right: 30, top: 20),
                  height: 80,
                  width: MediaQuery.of(context).size.width,
                  child: Material(
                    color: Colors.white.withOpacity(0.9),
                    elevation: 5.0,
                    shadowColor: Colors.black,
                    borderRadius: BorderRadius.circular(14.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Flexible(
                          flex: 3,
                          fit: FlexFit.loose,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: CircleAvatar(
                              minRadius: 28,
                              maxRadius: 30,
                              backgroundColor: Color.fromRGBO(3, 90, 166, 0.8),
                              child: Icon(
                                Icons.file_download,
                                color: Colors.white,
                                size: 28,
                              ),
                            ),
                          ),
                        ),
                        Flexible(
                          flex: 7,
                          fit: FlexFit.tight,
                          child: GestureDetector(
                            onTap: () => _downloadForm(context),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Center(
                                child: Text(
                                  'Descargar formulario para matricula'
                                      .toUpperCase(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: Fonts.primaryFont,
                                      fontWeight: FontWeight.w600,
                                      color: Color.fromRGBO(3, 90, 166, 1),
                                      fontSize: 18),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                // Texto obligatorio
                Container(
                  width: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                  child: Material(
                    color: Colors.transparent,
                    elevation: 5.0,
                    shadowColor: Color.fromRGBO(3, 90, 166, 0.3),
                    borderRadius: BorderRadius.circular(12.0),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 20),
                      child: Column(
                        children: [
                          Text(
                            'Las modalidades flexibles son un servicio educativo gratuito que se ofrece a jovenes y adultos con sobreedad para que continuen y finalicen sus estudios completamente gratis. Se ofrece a la poblacion la oportunidad de estudiar su Primer y Segundo año de Bachillerato general en la Sede del C.E ${sede.name} que funciona unicamente los dias Domingos de 7:30am a 4:00pm en la modalidad semipresencial.',
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: Fonts.primaryFont,
                                fontSize: 16),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            'COORDINADOR DE SEDE:\n LIC. MARIO ROSALES',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: Fonts.primaryFont,
                                fontSize: 18),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      )),
    );
  }

  void _downloadForm(BuildContext context) {
    // GlobalWidgets.showProgress(context, 'Descargando Formulario');
    // Future.delayed(Duration(seconds: 2), () {
    //   GlobalWidgets.hideProgress(context);
    //   GlobalWidgets.showInfoToast('Formulario descargado exitosamente',
    //       Colors.white.withOpacity(0.9), MyColors.mainColor);
    // });
    reservationBloc.add(
        DownloadNewStudentForm(NetworkUtils.defaultSignUpFormLink, context));
  }
}
