import 'package:app_margarita_duran/animations/FadeAnimation.dart';
import 'package:app_margarita_duran/bloc/authenticationBloc.dart';
import 'package:app_margarita_duran/bloc/reservationBloc.dart';
import 'package:app_margarita_duran/events/reservationEvents.dart';
import 'package:app_margarita_duran/global/styles.dart';
import 'package:app_margarita_duran/global/widgets.dart'; //used as GlobalWidgets
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

String _sexo = 'Seleccionar',
    _identidadGenero = 'Heterosexual',
    _nacionalidad = '',
    _estadoFamiliar = 'Soltero(a)',
    _medioDeTransporte = 'Propio',
    _trabaja = '',
    _zonaDeRes = '',
    _convivencia = 'Seleccionar',
    _dependencia = 'Seleccionar',
    _institucion = 'Seleccionar';
bool _discapacidad = false, _retornado = false, _tieneHijos = false;
List<String> _identidadesDeGenero = [
  'Heterosexual',
  'Lesbiana',
  'Bisexual',
  'Transexual',
  'Gay',
  'Intersexual'
];
List<String> _estadosFamiliar = [
  'Soltero(a)',
  'Casado(a)',
  'Acompañado(a)',
];
List<String> _mediosDeTransporte = [
  'Propio',
  'Público',
  'Familiar',
  'Profesional',
  'Otro',
];
List<String> _tiposConvivencia = [
  'Seleccionar',
  'Con Madre',
  'Con Padre',
  'Padre y Madre',
  'Con Familiar',
  'No vive con Familiares',
  'Otros'
];
List<String> _dependendenciaEconomica = [
  'Seleccionar',
  'Padre',
  'Madre',
  'Padre y Madre',
  'Hermanos',
  'Otros'
];
List<String> _tipoDeInstitucion = [
  'Seleccionar',
  'Modalidades Flexibles',
  'Regular',
  'Extranjero',
];
DateTime _birthDate;

class ReservarMatriculaPage extends StatefulWidget {
  final AuthenticationBloc authenticationBloc;
  final ReservationBloc reservationBloc;

  const ReservarMatriculaPage(
      {Key key,
      @required this.authenticationBloc,
      @required this.reservationBloc})
      : super(key: key);

  @override
  _ReservarMatriculaPageState createState() => _ReservarMatriculaPageState();
}

class _ReservarMatriculaPageState extends State<ReservarMatriculaPage> {
  ReservationBloc get _reservationBloc => widget.reservationBloc;
  TextEditingController codSedeController = TextEditingController();
  TextEditingController codCentroController = TextEditingController();
  TextEditingController nomSedeController = TextEditingController();
  TextEditingController nombreController = TextEditingController();
  TextEditingController apellidoController = TextEditingController();
  TextEditingController numDUIController = TextEditingController();
  TextEditingController numNIEController = TextEditingController();
  TextEditingController distanciaController = TextEditingController();
  TextEditingController ocupacionController = TextEditingController();
  TextEditingController discapacidadController = TextEditingController();
  TextEditingController paisRetornoController = TextEditingController();
  TextEditingController direccionController = TextEditingController();
  TextEditingController telResidenciaController = TextEditingController();
  TextEditingController telCelularController = TextEditingController();
  TextEditingController telTrabajoController = TextEditingController();
  TextEditingController correoController = TextEditingController();
  TextEditingController numFamiliaresController = TextEditingController();
  TextEditingController cantHijosController = TextEditingController();
  TextEditingController edadHijoController = TextEditingController();
  TextEditingController sexoHijoController = TextEditingController();
  TextEditingController ultimoGradoController = TextEditingController();
  TextEditingController anioUltimoGradoController = TextEditingController();
  TextEditingController codInstitucionController = TextEditingController();
  TextEditingController nomCentroController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Color.fromRGBO(255, 255, 255, 1),
          elevation: 0,
          actions: [
            IconButton(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                iconSize: 28,
                tooltip: 'Retroceder',
                icon: Icon(Icons.arrow_back),
                color: Color.fromRGBO(3, 90, 166, 1),
                onPressed: () {
                  _reservationBloc.add(ReservationStarted());
                }),
          ]),
      resizeToAvoidBottomPadding: true,
      body: SafeArea(
          child: Stack(
        children: [
          Positioned(
            child: ListView(
              children: [
                // Titulo y logo
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      flex: 3,
                      fit: FlexFit.loose,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 15.0, right: 10),
                        child: Container(
                          height: 80,
                          width: 85,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      AssetImage('assets/images/badge.png'))),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 6,
                      fit: FlexFit.tight,
                      child: Container(
                        padding: EdgeInsets.only(bottom: 20),
                        width: MediaQuery.of(context).size.width,
                        child: FittedBox(
                          child: Text(
                            'RESERVAR MATRICULA',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontFamily: Fonts.secondaryFont,
                                fontSize: 24,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                // Indicaciones
                Center(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    child: Text(
                      'POR FAVOR INGRESAR TODA LA INFORMACIÓN SOLICITADA',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16,
                          color: Color.fromRGBO(3, 90, 166, 1),
                          fontFamily: Fonts.primaryFont),
                    ),
                  ),
                ),
                // Datos de Sede
                _datosDeSede(context),
                // Fin de datos de la sede

                // Datos Personales
                _datosPersonales(context),
                // Fin de datos Personales.

                //Datos de residencia
                _datosDeResidencia(context),
                //Fin de datos de residencia

                //Datos sobre situacion familiar
                _situacionFamiliar(context),
                //Fin de datos sobre situacion familiar

                //Estudios Realizados
                _estudiosRealizados(context),
                //Fin de estudios realizados
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 5.0, horizontal: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GlobalWidgets.secondaryBtn('Anterior', context,
                          _reservationBloc, ReservationStarted()),
                      GlobalWidgets.mainBtn(
                          'Enviar',
                          context,
                          _reservationBloc,
                          SendOnlineReservationForm(
                              anioDeUltimoCurso: anioUltimoGradoController.text,
                              apellido: apellidoController.text,
                              codCentro: codCentroController.text,
                              codInstitucion: codInstitucionController.text,
                              codSede: codSedeController.text,
                              convivencia: _convivencia,
                              dependenciaEconomica: _dependencia,
                              direccion: direccionController.text,
                              dui: numDUIController.text,
                              estadoFamiliar: _estadoFamiliar,
                              fechaNac: _birthDate != null
                                  ? DateFormat('yyyy-MM-dd – kk:mm')
                                      .format(_birthDate)
                                  : 'No especificado',
                              idenGenero: _identidadGenero,
                              medioTransporte: _medioDeTransporte,
                              nacionalidad: _nacionalidad,
                              nie: numNIEController.text,
                              nombre: nombreController.text,
                              nombreDelCentro: nomCentroController.text,
                              nombreSede: nomSedeController.text,
                              nombreyEdadHijos: null,
                              numHijos: null,
                              numMiembrosFamilia: numFamiliaresController.text,
                              poseeDiscapacida: _discapacidad ? 'Si' : 'No',
                              discapacidad: _discapacidad
                                  ? discapacidadController.text
                                  : 'N/A',
                              retornado: _retornado ? 'Si' : 'No',
                              paisRetorno: _retornado
                                  ? paisRetornoController.text
                                  : 'N/A',
                              sexo: _sexo,
                              telCelular: telCelularController.text,
                              telResidencia: telResidenciaController.text,
                              telTrabajo: telTrabajoController.text,
                              tipoDeInstitucion: _institucion,
                              trabaja: _trabaja,
                              ultimnoGradoCursado: ultimoGradoController.text,
                              zonaResidencia: _zonaDeRes))
                    ],
                  ),
                ),
                SizedBox(height: 15.0),
              ],
            ),
          ),
        ],
      )),
    );
  }

  Future<DateTime> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime(2010),
        firstDate: new DateTime(1910),
        lastDate: new DateTime.now());
    if (picked != null) {
      return picked;
    } else {
      return null;
    }
  }

  _datosDeSede(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        child: Column(
          children: [
            Row(
              children: [
                Flexible(
                  fit: FlexFit.loose,
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Icon(Icons.account_balance,
                        size: 28, color: Color.fromRGBO(3, 90, 166, 1)),
                  ),
                ),
                Flexible(
                  fit: FlexFit.loose,
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Text(
                      'DATOS DE SEDE',
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
            // Campos para ingresar informacion de la sede
            SizedBox(
              height: 5,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(143, 148, 251, .2),
                        blurRadius: 20.0,
                        offset: Offset(0, 10))
                  ]),
              child: Column(
                children: [
                  // Codigo del centro
                  GlobalWidgets.customFormField(codCentroController,
                      labeltxt: 'Codigo de centro',
                      leadingIcon: Icons.dialpad,
                      hintTxt: 'Ej. 789456'),
                  // Codigo de sede
                  GlobalWidgets.customFormField(codSedeController,
                      labeltxt: 'Código de sede',
                      leadingIcon: Icons.dialpad,
                      hintTxt: 'Ej. 789456'),
                  // Nombre de sede
                  GlobalWidgets.customFormField(nomSedeController,
                      labeltxt: 'Nombre de la sede',
                      leadingIcon: Icons.text_fields,
                      hintTxt: 'Ej. Sede Margarita Dúran'),
                ],
              ),
            ),
          ],
        ),
      );

  _datosPersonales(BuildContext context) => Container(
        margin: EdgeInsets.only(left: 30, right: 30, top: 0, bottom: 5),
        child: Column(
          children: [
            Row(
              children: [
                Flexible(
                  fit: FlexFit.loose,
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Icon(Icons.perm_identity,
                        size: 28, color: Color.fromRGBO(3, 90, 166, 1)),
                  ),
                ),
                Flexible(
                  fit: FlexFit.loose,
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Text(
                      'DATOS PERSONALES',
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(143, 148, 251, .2),
                        blurRadius: 20.0,
                        offset: Offset(0, 10))
                  ]),
              child: Column(
                children: [
                  // Nombre
                  GlobalWidgets.customFormField(nombreController,
                      labeltxt: 'Nombre',
                      hintTxt: 'Ej. Juan Pérez',
                      leadingIcon: Icons.text_fields),
                  // Apellido
                  GlobalWidgets.customFormField(apellidoController,
                      labeltxt: 'Apellido',
                      hintTxt: 'Ej. Vega Sibrian',
                      leadingIcon: Icons.text_fields),
                  // Sexo
                  Row(
                    children: <Widget>[
                      Flexible(
                          fit: FlexFit.loose,
                          flex: 5,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 12.0),
                            child: Row(
                              children: [
                                Flexible(
                                  flex: 2,
                                  fit: FlexFit.loose,
                                  child: Icon(
                                    Icons.sentiment_very_satisfied,
                                    color: Color.fromRGBO(37, 117, 191, .8),
                                  ),
                                ),
                                Flexible(
                                    flex: 3,
                                    fit: FlexFit.loose,
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(left: 12.0),
                                      child: Text(
                                        'Sexo: ',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: Fonts.primaryFont,
                                            color: Color.fromRGBO(
                                                37, 117, 191, .8)),
                                      ),
                                    )),
                              ],
                            ),
                          )),
                      Flexible(
                        flex: 5,
                        fit: FlexFit.tight,
                        child: GlobalWidgets.radioOption(
                            groupValue: _sexo,
                            value: 'Masculino',
                            labelText: 'Masculino',
                            onChanged: (value) => setState(() {
                                  _sexo = value;
                                })),
                      ),
                      Flexible(
                        flex: 5,
                        fit: FlexFit.tight,
                        child: GlobalWidgets.radioOption(
                            groupValue: _sexo,
                            value: 'Femenino',
                            labelText: 'Femenino',
                            onChanged: (value) => setState(() {
                                  _sexo = value;
                                })),
                      )
                    ],
                  ),
                  // Fecha de nacimiento
                  InkWell(
                    onTap: () {
                      _selectDate(context).then((value) => setState(() {
                            _birthDate = value;
                          }));
                    },
                    child: InputDecorator(
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          prefixIcon: Icon(
                            Icons.calendar_today,
                            color: Color.fromRGBO(37, 117, 191, .8),
                          ),
                          contentPadding: EdgeInsets.only(
                              top: 3, bottom: 2, left: 4, right: 4),
                          labelText: 'Fecha de Nacimiento',
                          labelStyle: TextStyle(
                              fontSize: 16,
                              fontFamily: Fonts.primaryFont,
                              color: Color.fromRGBO(37, 117, 191, .8)),
                          enabled: true),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          FittedBox(
                            child: Text(
                              _birthDate == null
                                  ? 'Seleccione fecha de nacimiento'
                                  : _birthDate.toString(),
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: Fonts.primaryFont,
                                  color: Colors.black54),
                            ),
                          ),
                          Icon(Icons.arrow_drop_down,
                              color: Theme.of(context).brightness ==
                                      Brightness.light
                                  ? Color.fromRGBO(37, 117, 191, .8)
                                  : Colors.white70),
                        ],
                      ),
                    ),
                  ),
                  // Numero de DUI
                  GlobalWidgets.customFormField(numDUIController,
                      labeltxt: 'Ingrese su numero de DUI',
                      hintTxt: 'Ej. 12345678-9',
                      leadingIcon: Icons.dialpad),
                  // Numero de NIE
                  GlobalWidgets.customFormField(numNIEController,
                      labeltxt: 'Ingrese su numero de NIE',
                      hintTxt: 'Ej. 0145-451278-158-4',
                      leadingIcon: Icons.dialpad),
                  // Identidad de genero
                  Row(
                    children: [
                      Flexible(
                          flex: 6,
                          fit: FlexFit.loose,
                          child: Row(
                            children: [
                              Flexible(
                                fit: FlexFit.loose,
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 11.0, right: 12.0),
                                  child: Icon(Icons.sentiment_very_satisfied,
                                      size: 24,
                                      color: Color.fromRGBO(37, 117, 191, .8)),
                                ),
                              ),
                              Flexible(
                                fit: FlexFit.loose,
                                flex: 6,
                                child: FittedBox(
                                  child: Text(
                                    'Identidad de género: ',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: Fonts.primaryFont,
                                        color:
                                            Color.fromRGBO(37, 117, 191, .8)),
                                  ),
                                ),
                              ),
                            ],
                          )),
                      Flexible(
                        flex: 4,
                        fit: FlexFit.loose,
                        child: GlobalWidgets.dropdownBtn(
                            _identidadGenero,
                            _identidadesDeGenero,
                            (value) => setState(() {
                                  _identidadGenero = value;
                                })),
                      )
                    ],
                  ),
                  // Nacionalidad
                  Column(
                    children: [
                      Row(
                        children: [
                          Flexible(
                            flex: 2,
                            fit: FlexFit.loose,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: Icon(
                                Icons.outlined_flag,
                                color: Color.fromRGBO(37, 117, 191, .8),
                              ),
                            ),
                          ),
                          Flexible(
                              flex: 3,
                              fit: FlexFit.loose,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: Text(
                                  'Nacionalidad: ',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Fonts.primaryFont,
                                      color: Color.fromRGBO(37, 117, 191, .8)),
                                ),
                              )),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 24.0, top: 5.0),
                        child: Row(
                          children: [
                            Flexible(
                                flex: 5,
                                fit: FlexFit.loose,
                                child: GlobalWidgets.radioOption(
                                    groupValue: _nacionalidad,
                                    value: 'Salvadoreña',
                                    labelText: 'Salvadoreña',
                                    onChanged: (value) => setState(() {
                                          _nacionalidad = value;
                                        }))),
                            Flexible(
                              flex: 5,
                              fit: FlexFit.loose,
                              child: GlobalWidgets.radioOption(
                                  groupValue: _nacionalidad,
                                  value: 'Extranjera',
                                  labelText: 'Extranjera',
                                  onChanged: (value) => setState(() {
                                        _nacionalidad = value;
                                      })),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  //Fin de nacionalidad

                  //Estado Familiar
                  Row(
                    children: [
                      Flexible(
                          flex: 5,
                          fit: FlexFit.loose,
                          child: Row(
                            children: [
                              Flexible(
                                fit: FlexFit.loose,
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 11.0, right: 12.0),
                                  child: Icon(Icons.group,
                                      size: 24,
                                      color: Color.fromRGBO(37, 117, 191, .8)),
                                ),
                              ),
                              Flexible(
                                fit: FlexFit.loose,
                                flex: 4,
                                child: FittedBox(
                                  child: Text(
                                    'Estado Familiar: ',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: Fonts.primaryFont,
                                        color:
                                            Color.fromRGBO(37, 117, 191, .8)),
                                  ),
                                ),
                              ),
                            ],
                          )),
                      Flexible(
                        flex: 4,
                        fit: FlexFit.loose,
                        child: GlobalWidgets.dropdownBtn(
                            _estadoFamiliar,
                            _estadosFamiliar,
                            (value) => setState(() {
                                  _estadoFamiliar = value;
                                })),
                      )
                    ],
                  ),

                  //Medio de transporte
                  Row(
                    children: [
                      Flexible(
                          flex: 5,
                          fit: FlexFit.loose,
                          child: Row(
                            children: [
                              Flexible(
                                fit: FlexFit.loose,
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 11.0, right: 12.0),
                                  child: Icon(Icons.directions_car,
                                      size: 24,
                                      color: Color.fromRGBO(37, 117, 191, .8)),
                                ),
                              ),
                              Flexible(
                                fit: FlexFit.loose,
                                flex: 5,
                                child: FittedBox(
                                  child: Text(
                                    'Medio De Transporte: ',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: Fonts.primaryFont,
                                        color:
                                            Color.fromRGBO(37, 117, 191, .8)),
                                  ),
                                ),
                              ),
                            ],
                          )),
                      Flexible(
                        flex: 3,
                        fit: FlexFit.loose,
                        child: GlobalWidgets.dropdownBtn(
                            _medioDeTransporte,
                            _mediosDeTransporte,
                            (value) => setState(() {
                                  _medioDeTransporte = value;
                                })),
                      )
                    ],
                  ),

                  //Distancia en km entre la sede y el centro
                  GlobalWidgets.customFormField(distanciaController,
                      labeltxt: 'Distancia en kilometros para llegar al centro',
                      hintTxt: 'Ingrese el numero de kilometros',
                      leadingIcon: Icons.directions_run,
                      fontSize: 14),

                  //Trabaja
                  Row(
                    children: <Widget>[
                      Flexible(
                          fit: FlexFit.loose,
                          flex: 7,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 12.0),
                            child: Row(
                              children: [
                                Flexible(
                                  flex: 2,
                                  fit: FlexFit.loose,
                                  child: Icon(
                                    Icons.work,
                                    color: Color.fromRGBO(37, 117, 191, .8),
                                  ),
                                ),
                                Flexible(
                                    flex: 3,
                                    fit: FlexFit.loose,
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(left: 12.0),
                                      child: Text(
                                        'Trabaja: ',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: Fonts.primaryFont,
                                            color: Color.fromRGBO(
                                                37, 117, 191, .8)),
                                      ),
                                    )),
                              ],
                            ),
                          )),
                      Flexible(
                        flex: 4,
                        fit: FlexFit.tight,
                        child: GlobalWidgets.radioOption(
                            groupValue: _trabaja,
                            value: 'Si',
                            labelText: 'Si',
                            onChanged: (value) => setState(() {
                                  _trabaja = value;
                                })),
                      ),
                      Flexible(
                        flex: 4,
                        fit: FlexFit.tight,
                        child: GlobalWidgets.radioOption(
                            groupValue: _trabaja,
                            value: 'No',
                            labelText: 'No',
                            onChanged: (value) => setState(() {
                                  _trabaja = value;
                                })),
                      )
                    ],
                  ),

                  //Ocupacion
                  GlobalWidgets.customFormField(ocupacionController,
                      labeltxt: 'Ingrese su ocupacion',
                      hintTxt: 'Ingrese su ocupacion',
                      leadingIcon: Icons.work,
                      fontSize: 16),

                  //Discapacidad
                  Padding(
                    padding: const EdgeInsets.only(top: 6.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Flexible(
                              flex: 2,
                              fit: FlexFit.loose,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: Icon(
                                  Icons.accessible,
                                  color: Color.fromRGBO(37, 117, 191, .8),
                                ),
                              ),
                            ),
                            Flexible(
                                flex: 3,
                                fit: FlexFit.loose,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 12.0),
                                  child: Text(
                                    'Posee Discapacidad: ',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: Fonts.primaryFont,
                                        color:
                                            Color.fromRGBO(37, 117, 191, .8)),
                                  ),
                                )),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 48.0, top: 5.0),
                          child: Row(
                            children: [
                              Flexible(
                                  flex: 5,
                                  fit: FlexFit.loose,
                                  child: GlobalWidgets.radioOption(
                                      groupValue: _discapacidad,
                                      value: true,
                                      labelText: 'Si',
                                      onChanged: (value) => setState(() {
                                            _discapacidad = value;
                                          }))),
                              Flexible(
                                flex: 5,
                                fit: FlexFit.loose,
                                child: GlobalWidgets.radioOption(
                                    groupValue: _discapacidad,
                                    value: false,
                                    labelText: 'No',
                                    onChanged: (value) => setState(() {
                                          _discapacidad = value;
                                        })),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),

                  //Tipo de discapacidad
                  _discapacidad
                      ? FadeAnimation(
                          0.2,
                          GlobalWidgets.customFormField(discapacidadController,
                              labeltxt: 'Ingrese el tipo de discapacidad',
                              hintTxt: 'Tipo de discapacidad',
                              leadingIcon: Icons.accessible,
                              fontSize: 16))
                      : SizedBox(
                          height: 0,
                        ),

                  //Retornado
                  Row(
                    children: <Widget>[
                      Flexible(
                          fit: FlexFit.loose,
                          flex: 9,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 12.0),
                            child: Row(
                              children: [
                                Flexible(
                                  flex: 2,
                                  fit: FlexFit.loose,
                                  child: Icon(
                                    Icons.flight_land,
                                    color: Color.fromRGBO(37, 117, 191, .8),
                                  ),
                                ),
                                Flexible(
                                    flex: 3,
                                    fit: FlexFit.loose,
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(left: 12.0),
                                      child: Text(
                                        'Retornado: ',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: Fonts.primaryFont,
                                            color: Color.fromRGBO(
                                                37, 117, 191, .8)),
                                      ),
                                    )),
                              ],
                            ),
                          )),
                      Flexible(
                        flex: 4,
                        fit: FlexFit.tight,
                        child: GlobalWidgets.radioOption(
                            groupValue: _retornado,
                            value: true,
                            labelText: 'Si',
                            onChanged: (value) => setState(() {
                                  _retornado = value;
                                })),
                      ),
                      Flexible(
                        flex: 4,
                        fit: FlexFit.tight,
                        child: GlobalWidgets.radioOption(
                            groupValue: _retornado,
                            value: false,
                            labelText: 'No',
                            onChanged: (value) => setState(() {
                                  _retornado = value;
                                })),
                      )
                    ],
                  ),

                  //Pais de retorno
                  _retornado
                      ? FadeAnimation(
                          0.2,
                          GlobalWidgets.customFormField(paisRetornoController,
                              labeltxt: 'Ingrese Pais de retorno',
                              hintTxt: 'Pais de retorno',
                              leadingIcon: Icons.accessible,
                              fontSize: 16))
                      : SizedBox(
                          height: 0,
                        ),
                  SizedBox(
                    height: 5.0,
                  )
                ],
              ),
            ),
          ],
        ),
      );

  _datosDeResidencia(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        child: Column(
          children: [
            Row(
              children: [
                Flexible(
                  fit: FlexFit.loose,
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Icon(Icons.hotel,
                        size: 28, color: Color.fromRGBO(3, 90, 166, 1)),
                  ),
                ),
                Flexible(
                  fit: FlexFit.loose,
                  flex: 6,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Text(
                      'DATOS DE RESIDENCIA',
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
            // Campos para ingresar informacion de residencia
            SizedBox(
              height: 5,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(143, 148, 251, .2),
                        blurRadius: 20.0,
                        offset: Offset(0, 10))
                  ]),
              child: Column(
                children: [
                  // Direccion
                  GlobalWidgets.customFormField(direccionController,
                      labeltxt: 'Dirección',
                      leadingIcon: Icons.location_on,
                      hintTxt: 'Ingrese direccion completa'),
                  //Zona de Residencia
                  Padding(
                    padding: const EdgeInsets.only(top: 6.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Flexible(
                              flex: 2,
                              fit: FlexFit.loose,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: Icon(
                                  Icons.my_location,
                                  color: Color.fromRGBO(37, 117, 191, .8),
                                ),
                              ),
                            ),
                            Flexible(
                                flex: 3,
                                fit: FlexFit.loose,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 12.0),
                                  child: Text(
                                    'Zona de residencia: ',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: Fonts.primaryFont,
                                        color:
                                            Color.fromRGBO(37, 117, 191, .8)),
                                  ),
                                )),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 48.0, top: 5.0),
                          child: Row(
                            children: [
                              Flexible(
                                  flex: 5,
                                  fit: FlexFit.loose,
                                  child: GlobalWidgets.radioOption(
                                      groupValue: _zonaDeRes,
                                      value: 'Rural',
                                      labelText: 'Rural',
                                      onChanged: (value) => setState(() {
                                            _zonaDeRes = value;
                                          }))),
                              Flexible(
                                flex: 5,
                                fit: FlexFit.loose,
                                child: GlobalWidgets.radioOption(
                                    groupValue: _zonaDeRes,
                                    value: 'Urbano',
                                    labelText: 'Urbano',
                                    onChanged: (value) => setState(() {
                                          _zonaDeRes = value;
                                        })),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  // Telefono de residencia
                  GlobalWidgets.customFormField(telResidenciaController,
                      labeltxt: 'Telefono residencia',
                      leadingIcon: Icons.dialpad,
                      hintTxt: 'Telefono residencia'),
                  // Telefono celular
                  GlobalWidgets.customFormField(telCelularController,
                      labeltxt: 'Telefono celular',
                      leadingIcon: Icons.dialpad,
                      hintTxt: 'Telefono celular'),
                  // Telefono de trabajo
                  GlobalWidgets.customFormField(telTrabajoController,
                      labeltxt: 'Telefono de trabajo',
                      leadingIcon: Icons.dialpad,
                      hintTxt: 'Telefono de trabajo'),
                  // Correo electronico
                  GlobalWidgets.customFormField(correoController,
                      labeltxt: 'Correo Electronico',
                      leadingIcon: Icons.alternate_email,
                      hintTxt: 'Ingrese su correo electronico'),
                ],
              ),
            ),
          ],
        ),
      );

  _situacionFamiliar(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
        child: Column(
          children: [
            Row(
              children: [
                Flexible(
                  fit: FlexFit.loose,
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Icon(Icons.account_balance,
                        size: 28, color: Color.fromRGBO(3, 90, 166, 1)),
                  ),
                ),
                Flexible(
                  fit: FlexFit.loose,
                  flex: 7,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Text(
                      'DATOS SOBRE SITUACION FAMILIAR',
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
            // Campos para ingresar informacion de la sede
            SizedBox(
              height: 5,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(143, 148, 251, .2),
                        blurRadius: 20.0,
                        offset: Offset(0, 10))
                  ]),
              child: Column(
                children: [
                  // Direccion
                  GlobalWidgets.customFormField(numFamiliaresController,
                      labeltxt: 'Numero de miembros de la Familia',
                      leadingIcon: Icons.people_outline,
                      hintTxt: 'Ingrese el numero de familiares'),
                  // Convivencia
                  Row(
                    children: [
                      Flexible(
                          flex: 5,
                          fit: FlexFit.loose,
                          child: Row(
                            children: [
                              Flexible(
                                fit: FlexFit.loose,
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 11.0, right: 12.0),
                                  child: Icon(Icons.people,
                                      size: 24,
                                      color: Color.fromRGBO(37, 117, 191, .8)),
                                ),
                              ),
                              Flexible(
                                fit: FlexFit.loose,
                                flex: 4,
                                child: FittedBox(
                                  child: Text(
                                    'Convivencia: ',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: Fonts.primaryFont,
                                        color:
                                            Color.fromRGBO(37, 117, 191, .8)),
                                  ),
                                ),
                              ),
                            ],
                          )),
                      Flexible(
                        flex: 7,
                        fit: FlexFit.loose,
                        child: GlobalWidgets.dropdownBtn(
                            _convivencia,
                            _tiposConvivencia,
                            (value) => setState(() {
                                  _convivencia = value;
                                })),
                      )
                    ],
                  ),
                  //De quien depende economicamente
                  Padding(
                    padding: const EdgeInsets.only(top: 6.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Flexible(
                              flex: 2,
                              fit: FlexFit.loose,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: Icon(
                                  Icons.monetization_on,
                                  color: Color.fromRGBO(37, 117, 191, .8),
                                ),
                              ),
                            ),
                            Flexible(
                                flex: 3,
                                fit: FlexFit.loose,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 12.0),
                                  child: Text(
                                    'De quien depende economicamente: ',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: Fonts.primaryFont,
                                        color:
                                            Color.fromRGBO(37, 117, 191, .8)),
                                  ),
                                )),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 60.0, top: 5.0),
                          child: GlobalWidgets.dropdownBtn(
                              _dependencia,
                              _dependendenciaEconomica,
                              (value) => setState(() {
                                    _dependencia = value;
                                  })),
                        )
                      ],
                    ),
                  ),
                  // Tiene hijos
                  Padding(
                    padding: const EdgeInsets.only(top: 6.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Flexible(
                              flex: 2,
                              fit: FlexFit.loose,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: Icon(
                                  Icons.child_friendly,
                                  color: Color.fromRGBO(37, 117, 191, .8),
                                ),
                              ),
                            ),
                            Flexible(
                                flex: 3,
                                fit: FlexFit.loose,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 12.0),
                                  child: Text(
                                    'Tiene hijos ',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: Fonts.primaryFont,
                                        color:
                                            Color.fromRGBO(37, 117, 191, .8)),
                                  ),
                                )),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 48.0, top: 5.0),
                          child: Row(
                            children: [
                              Flexible(
                                  flex: 5,
                                  fit: FlexFit.loose,
                                  child: GlobalWidgets.radioOption(
                                      groupValue: _tieneHijos,
                                      value: true,
                                      labelText: 'Si',
                                      onChanged: (value) => setState(() {
                                            _tieneHijos = value;
                                          }))),
                              Flexible(
                                flex: 5,
                                fit: FlexFit.loose,
                                child: GlobalWidgets.radioOption(
                                    groupValue: _tieneHijos,
                                    value: false,
                                    labelText: 'No',
                                    onChanged: (value) => setState(() {
                                          _tieneHijos = value;
                                        })),
                              )
                            ],
                          ),
                        ),
                        // Cantidad de hijos
                        _tieneHijos
                            ? FadeAnimation(
                                0.2,
                                GlobalWidgets.customFormField(
                                    cantHijosController,
                                    labeltxt: 'Cantidad de hijos',
                                    leadingIcon: Icons.child_care,
                                    hintTxt: 'Cantidad de hijos'))
                            : SizedBox(height: 0),
                      ],
                    ),
                  ),
                  _tieneHijos
                      ? FadeAnimation(0.2, _detalleHijos(context, 3))
                      : SizedBox(
                          height: 0,
                        )
                ],
              ),
            ),
          ],
        ),
      );

  Column _detalleHijos(BuildContext context, int hijos) {
    List<Widget> widgetHijos = [];
    for (var i = 0; i < hijos; i++) {
      widgetHijos.add(Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Flexible(
            fit: FlexFit.loose,
            flex: 5,
            child: GlobalWidgets.customFormField(edadHijoController,
                labeltxt: 'Edad del hijo ' + (i + 1).toString(),
                leadingIcon: Icons.child_care,
                hintTxt: 'Ingrese la edad de su hijo ' + (i + 1).toString()),
          ),
          Flexible(
            fit: FlexFit.loose,
            flex: 5,
            child: GlobalWidgets.customFormField(sexoHijoController,
                labeltxt: 'Sexo del hijo ' + (i + 1).toString(),
                leadingIcon: Icons.text_fields,
                hintTxt: 'Sexo del hijo ' + (i + 1).toString()),
          )
        ],
      ));
    }

    return Column(
      children: widgetHijos,
    );
  }

  _estudiosRealizados(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
        child: Column(
          children: [
            Row(
              children: [
                Flexible(
                  fit: FlexFit.loose,
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Icon(Icons.school,
                        size: 28, color: Color.fromRGBO(3, 90, 166, 1)),
                  ),
                ),
                Flexible(
                  fit: FlexFit.loose,
                  flex: 7,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Text(
                      'ESTUDIOS REALIZADOS',
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: Fonts.primaryFont,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
            // Campos para ingresar informacion de la sede
            SizedBox(
              height: 5,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(143, 148, 251, .2),
                        blurRadius: 20.0,
                        offset: Offset(0, 10))
                  ]),
              child: Column(
                children: [
                  // Codigo del centro
                  GlobalWidgets.customFormField(ultimoGradoController,
                      labeltxt: 'Ultimo grado cursado',
                      leadingIcon: Icons.folder_special,
                      hintTxt: 'Ingrese el ultimo grado completado'),
                  // Codigo de sede
                  GlobalWidgets.customFormField(anioUltimoGradoController,
                      labeltxt: 'Año en el que curso su ultimo grado',
                      leadingIcon: Icons.schedule,
                      hintTxt: 'Año en el que curso su ultimo grado'),
                  Padding(
                    padding: const EdgeInsets.only(top: 6.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Flexible(
                              flex: 2,
                              fit: FlexFit.loose,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: Icon(
                                  Icons.monetization_on,
                                  color: Color.fromRGBO(37, 117, 191, .8),
                                ),
                              ),
                            ),
                            Flexible(
                                flex: 5,
                                fit: FlexFit.loose,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 12.0),
                                  child: Text(
                                    'Institucion en que lo curso: ',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: Fonts.primaryFont,
                                        color:
                                            Color.fromRGBO(37, 117, 191, .8)),
                                  ),
                                )),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 50.0, top: 5.0),
                          child: GlobalWidgets.dropdownBtn(
                              _institucion,
                              _tipoDeInstitucion,
                              (value) => setState(() {
                                    _institucion = value;
                                  })),
                        )
                      ],
                    ),
                  ),
                  // Nombre de sede
                  GlobalWidgets.customFormField(codInstitucionController,
                      labeltxt: 'Codigo de la institucion',
                      leadingIcon: Icons.grain,
                      hintTxt: 'Codigo de la institucion'),
                  // Nombre de sede
                  GlobalWidgets.customFormField(nomCentroController,
                      labeltxt: 'Nombre del centro educativo',
                      leadingIcon: Icons.account_balance,
                      hintTxt: 'Ej. Sede Margarita Dúran'),
                ],
              ),
            ),
          ],
        ),
      );
}
