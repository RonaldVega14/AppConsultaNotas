import 'package:flutter/material.dart';

class Fonts {
  static String get primaryFont => 'SourceSansPro';
  static String get secondaryFont => 'Montserrat';
}

class MyColors {
  static get mainColor => Color.fromRGBO(3, 90, 166, 1);
  static get secondaryColor => Color.fromRGBO(37, 117, 191, .6);
}

class Styles {
  static TextStyle get mainStyle => TextStyle(
        fontFamily: Fonts.primaryFont,
        fontSize: 16,
      );
}
