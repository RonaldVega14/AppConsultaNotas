import 'package:app_margarita_duran/bloc/authenticationBloc.dart';
import 'package:app_margarita_duran/bloc/homeBloc.dart';
import 'package:app_margarita_duran/events/authenticationEvents.dart';
import 'package:app_margarita_duran/events/homeEvents.dart';
import 'package:app_margarita_duran/global/styles.dart';
import 'package:app_margarita_duran/models/modulesModel.dart';
import 'package:app_margarita_duran/models/studentModel.dart';
import 'package:app_margarita_duran/models/subjectModel.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class GlobalWidgets {
  // Boton principal (Color azul oscuro)
  static mainBtn(
          String text, BuildContext context, dynamic bloc, dynamic event) =>
      GestureDetector(
        onTap: () {
          //Navigator.of(context).pop(true);
          bloc.add(event);
          // Navigator.push(
          //     context, MaterialPageRoute(builder: (context) => route));
        },
        child: Container(
          constraints: BoxConstraints(minWidth: 90, maxWidth: 135),
          height: 35,
          padding: EdgeInsets.symmetric(horizontal: 25),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              color: Color.fromRGBO(3, 90, 166, 1)),
          child: Center(
            child: FittedBox(
              child: Text(
                text,
                style: TextStyle(
                    fontFamily: Fonts.secondaryFont,
                    fontSize: 14,
                    color: Colors.white,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ),
      );

  // Boton secundario (Color azul claro)
  static secondaryBtn(
          String text, BuildContext context, dynamic bloc, dynamic event) =>
      GestureDetector(
        onTap: () {
          //Navigator.of(context).pop(true);
          bloc.add(event);
          // Navigator.push(
          //     context, MaterialPageRoute(builder: (context) => route));
        },
        child: Container(
          constraints: BoxConstraints(minWidth: 90, maxWidth: 135),
          height: 35,
          padding: EdgeInsets.symmetric(horizontal: 12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              color: Color.fromRGBO(64, 186, 213, 1)),
          child: Center(
            child: FittedBox(
              child: Text(
                text,
                style: TextStyle(
                    fontFamily: Fonts.secondaryFont,
                    fontSize: 14,
                    color: Colors.white,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ),
      );

  // Boton de reposicion (Color azul claro)
  static reposicionBtn(
          String text, BuildContext context, Module module, HomeBloc homeBloc,
          {bool isDisable = false}) =>
      MaterialButton(
        padding: EdgeInsets.symmetric(horizontal: 20),
        elevation: 4,
        onPressed: () {
          if (!isDisable) {
            homeBloc.add(ToRecuparationSubjectsPage(module));
          }
        },
        child: Container(
          margin: EdgeInsets.only(bottom: 20, top: 10),
          height: 40,
          padding: EdgeInsets.symmetric(horizontal: 12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              color: Color.fromRGBO(64, 186, 213, 1)),
          child: Center(
            child: Text(
              text,
              style: TextStyle(
                  fontFamily: Fonts.secondaryFont,
                  fontSize: 17,
                  color: Colors.white,
                  fontWeight: FontWeight.w400),
            ),
          ),
        ),
      );

  // Campo para ingresar texto
  static inputField(
          String hintTxt, labelTxt, TextEditingController controller) =>
      Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(143, 148, 251, .2),
                  blurRadius: 20.0,
                  offset: Offset(0, 10))
            ]),
        child: TextField(
          controller: controller,
          decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2),
              labelText: labelTxt.toUpperCase(),
              labelStyle: TextStyle(
                  fontFamily: Fonts.primaryFont,
                  fontWeight: FontWeight.bold,
                  color: Color.fromRGBO(37, 117, 191, .6)),
              border: InputBorder.none,
              hintText: hintTxt,
              hintStyle: TextStyle(
                  fontFamily: Fonts.primaryFont,
                  color: Color.fromRGBO(37, 117, 191, .6))),
        ),
      );

  //Dropdown btn
  static dropdownBtn(
          String _value, List<String> _listValues, Function onChanged) =>
      DropdownButton<String>(
        value: _value,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(
            fontSize: 16, fontFamily: Fonts.primaryFont, color: Colors.black54),
        onChanged: (String newValue) {
          onChanged(newValue);
        },
        items: _listValues.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: FittedBox(
                child: Padding(
              padding: const EdgeInsets.only(left: 2.0),
              child: Text(
                value,
                style: TextStyle(
                    fontSize: 16,
                    fontFamily: Fonts.primaryFont,
                    color: Colors.black54),
              ),
            )),
          );
        }).toList(),
      );

  // Row text with two columns. (This widget is used to create asignatura ans nota labels)
  static twoRowText({String firstText, String secondText}) => Row(
        children: [
          Text(
            firstText,
            style: TextStyle(
              fontFamily: Fonts.primaryFont,
              fontSize: 20,
            ),
          ),
          Text(
            secondText,
            style: TextStyle(
                fontFamily: Fonts.primaryFont,
                fontSize: 20,
                fontWeight: FontWeight.bold),
          )
        ],
      );

// Drawer Navigator
  static customDrawer(BuildContext context, AuthenticationBloc _authBloc,
          HomeBloc _homeBloc, Student student) =>
      Drawer(
          child: Center(
        child: ListView(children: [
          // Badge
          Container(
            margin: EdgeInsets.only(top: 25.0),
            height: 90,
            width: 90,
            child: Image.network(
              _authBloc.sede.logo,
              errorBuilder: (context, error, stackTrace) =>
                  Image.asset('assets/images/badge.png'),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 8),
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Bienvenido(a) a la sede del Centro Escolar ${_authBloc.sede.name}'
                  .toUpperCase(),
              style: TextStyle(
                  fontFamily: Fonts.secondaryFont,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
              overflow: TextOverflow.clip,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 20.0, right: 20, top: 10, bottom: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Flexible(
                  flex: 4,
                  fit: FlexFit.loose,
                  child: Text(
                    'Estudiante: ',
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: Fonts.primaryFont,
                    ),
                  ),
                ),
                Flexible(
                  flex: 7,
                  fit: FlexFit.loose,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      student.firstName + ' ' + student.lastName,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        fontFamily: Fonts.primaryFont,
                      ),
                      overflow: TextOverflow.clip,
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Flexible(
                  flex: 3,
                  fit: FlexFit.loose,
                  child: Text(
                    'Modalidad: ',
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: Fonts.primaryFont,
                    ),
                  ),
                ),
                Flexible(
                  flex: 6,
                  fit: FlexFit.loose,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      student.modality,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        fontFamily: Fonts.primaryFont,
                      ),
                      overflow: TextOverflow.clip,
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Flexible(
                  flex: 3,
                  fit: FlexFit.loose,
                  child: Text(
                    'Grado: ',
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: Fonts.primaryFont,
                    ),
                  ),
                ),
                Flexible(
                  flex: 7,
                  fit: FlexFit.loose,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      '${student.grade}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        fontFamily: Fonts.primaryFont,
                      ),
                      overflow: TextOverflow.clip,
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 20.0, right: 20.0, top: 5, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Flexible(
                  flex: 5,
                  fit: FlexFit.loose,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        flex: 3,
                        fit: FlexFit.loose,
                        child: Text(
                          'Año: ',
                          style: TextStyle(
                            fontSize: 16,
                            fontFamily: Fonts.primaryFont,
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 3,
                        fit: FlexFit.loose,
                        child: Text(
                          student.year,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            fontFamily: Fonts.primaryFont,
                          ),
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ),
                Flexible(
                  flex: 5,
                  fit: FlexFit.loose,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        flex: 3,
                        fit: FlexFit.loose,
                        child: Text(
                          'Sección: ',
                          style: TextStyle(
                            fontSize: 16,
                            fontFamily: Fonts.primaryFont,
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 3,
                        fit: FlexFit.loose,
                        child: Text(
                          '"${student.section}"',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            fontFamily: Fonts.primaryFont,
                          ),
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            color: Color.fromRGBO(238, 238, 238, 1),
            child: ListTile(
              onTap: () {
                Navigator.pop(context);
                _homeBloc.add(ToHomePage(student: _authBloc.student));
              },
              contentPadding: EdgeInsets.symmetric(horizontal: 20),
              leading: Icon(
                Icons.home_rounded,
                color: Color.fromRGBO(3, 90, 166, 1),
                size: 28,
              ),
              title: Text(
                'Inicio'.toUpperCase(),
                style: TextStyle(
                    fontFamily: Fonts.primaryFont,
                    fontSize: 17,
                    color: Color.fromRGBO(3, 90, 166, 1),
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            color: Color.fromRGBO(238, 238, 238, 1),
            child: ListTile(
              onTap: () =>
                  _homeBloc.add(ToStudentProfilePage(student: student)),
              contentPadding: EdgeInsets.symmetric(horizontal: 20),
              leading: Icon(
                Icons.person,
                color: Color.fromRGBO(3, 90, 166, 1),
                size: 28,
              ),
              title: Text(
                'Perfil'.toUpperCase(),
                style: TextStyle(
                    fontFamily: Fonts.primaryFont,
                    fontSize: 17,
                    color: Color.fromRGBO(3, 90, 166, 1),
                    fontWeight: FontWeight.bold),
                overflow: TextOverflow.clip,
              ),
            ),
          ),
          SizedBox(
            height: 130,
          ),
          GestureDetector(
            onTap: () => _authBloc.add(LoggedOut()),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(
                    Icons.power_settings_new,
                    color: Colors.redAccent,
                  ),
                  Text(
                    'Cerrar sesión'.toUpperCase(),
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: Fonts.primaryFont,
                        fontWeight: FontWeight.bold,
                        color: Colors.red),
                  )
                ],
              ),
            ),
          )
        ]),
      ));

  // This widget show a dialog message with a progress indicator
  static showProgress(BuildContext context, String msg) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => Center(
                child: Container(
              margin: EdgeInsets.symmetric(horizontal: 40.0),
              height: 220,
              child: Material(
                color: Colors.white.withOpacity(0.9),
                elevation: 5.0,
                shadowColor: Colors.black,
                borderRadius: BorderRadius.circular(14.0),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 15.0, vertical: 15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Flexible(
                          fit: FlexFit.tight,
                          flex: 3,
                          child: Text(
                            'Descargando formulario para reserva de nueva Matricula'
                                .toUpperCase(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontFamily: Fonts.primaryFont,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          )),
                      Flexible(
                          fit: FlexFit.loose,
                          flex: 6,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 15.0),
                            child: CircularProgressIndicator(
                              strokeWidth: 2,
                            ),
                          ))
                    ],
                  ),
                ),
              ),
            )));
  }

// This widget hides the dialog
  static hideProgress(BuildContext context) => Navigator.pop(context);

// This widget show a toast with information.
  static showInfoToast(String msg, Color bgColor, Color txtColor) =>
      Fluttertoast.showToast(
          msg: msg,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 3,
          backgroundColor: bgColor ?? Colors.white,
          textColor: txtColor ?? MyColors.mainColor,
          fontSize: 16.0);

  static showInfoForm(BuildContext context, dynamic bloc) {
    TextEditingController telefonoController = TextEditingController();
    TextEditingController telefonoEmergenciaController =
        TextEditingController();
    TextEditingController emailController = TextEditingController();

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => Center(
                child: Container(
              margin: EdgeInsets.symmetric(horizontal: 40.0, vertical: 75.0),
              child: Material(
                color: Colors.white.withOpacity(0.9),
                elevation: 5.0,
                shadowColor: Colors.black,
                borderRadius: BorderRadius.circular(14.0),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 15.0, vertical: 15.0),
                  child: ListView(
                    children: [
                      Text(
                        'Bienvenido(a) al sistema de notas de la sede del Centro Escolar Margarita Duran'
                            .toUpperCase(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: Fonts.primaryFont,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      ),
                      SizedBox(height: 15),
                      Text(
                        'Esta informacion es importante para llenar su perfil dentro del sistema, por favor leala cuidadosamente y conteste segun se le indique.',
                        style: TextStyle(
                            fontFamily: Fonts.secondaryFont,
                            color: Colors.black,
                            fontSize: 16),
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: 15),
                      formSectionHeader(
                          leadingIcon: Icons.verified_user,
                          headerText: 'Datos personales'),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(143, 148, 251, .2),
                                  blurRadius: 20.0,
                                  offset: Offset(0, 10))
                            ]),
                        child: Column(
                          children: [
                            customFormField(telefonoController,
                                labeltxt: 'Teléfono',
                                hintTxt: 'Ej. 75871548',
                                leadingIcon: Icons.dialpad),
                            customFormField(telefonoEmergenciaController,
                                labeltxt: 'Teléfono de emergencia',
                                hintTxt: 'Ej. 75845648',
                                leadingIcon: Icons.dialpad),
                            customFormField(emailController,
                                labeltxt: 'Correo de contacto',
                                hintTxt: 'Ej. margaritaduran@gmail.com',
                                leadingIcon: Icons.email)
                          ],
                        ),
                      ),
                      SizedBox(height: 35),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          mainBtn(
                              'Enviar',
                              context,
                              bloc,
                              InfoFormSubmit(
                                altPhoneNumber:
                                    telefonoEmergenciaController.text,
                                email: emailController.text,
                                phoneNumer: telefonoController.text,
                                context: context,
                              )),
                          secondaryBtn('Cancelar', context, bloc,
                              FirstTimeInfoFormCancel(context))
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            )));
  }

  static formSectionHeader(
          {@required IconData leadingIcon, @required String headerText}) =>
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Icon(leadingIcon,
                size: 28, color: Color.fromRGBO(3, 90, 166, 1)),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              headerText.toUpperCase(),
              style: TextStyle(
                  fontSize: 18,
                  fontFamily: Fonts.primaryFont,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ],
      );

  static customFormField(TextEditingController controller,
          {@required String labeltxt,
          @required String hintTxt,
          @required IconData leadingIcon,
          double fontSize = 16}) =>
      TextField(
        controller: controller,
        decoration: InputDecoration(
            prefixIcon: Icon(
              leadingIcon,
              color: Color.fromRGBO(37, 117, 191, .8),
            ),
            contentPadding:
                EdgeInsets.only(top: 6, bottom: 2, left: 4, right: 4),
            labelText: labeltxt,
            labelStyle: TextStyle(
                fontSize: fontSize,
                fontFamily: Fonts.primaryFont,
                color: Color.fromRGBO(37, 117, 191, .8)),
            border: InputBorder.none,
            hintText: hintTxt ?? labeltxt,
            hintStyle: TextStyle(
                fontFamily: Fonts.primaryFont,
                color: Color.fromRGBO(37, 117, 191, .6))),
      );

  static appBar(BuildContext context, AuthenticationBloc _authBloc) => AppBar(
      brightness: Brightness.light,
      backgroundColor: Color.fromRGBO(255, 255, 255, 1),
      elevation: 0,
      actions: [
        IconButton(
            padding: EdgeInsets.only(top: 8, bottom: 8, right: 10),
            iconSize: 28,
            tooltip: 'Cerrar sesión',
            icon: Icon(Icons.power_settings_new),
            color: Colors.redAccent,
            onPressed: () {
              _authBloc.add(LoggedOut());
            }),
      ],
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: Icon(
              Icons.menu,
              color: MyColors.mainColor,
            ),
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
          );
        },
      ));

  static Widget createAsignatura(
      {@required BuildContext context,
      @required String materia,
      @required String nota,
      @required bool aprobado}) {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 30, top: 20),
      height: 120,
      width: MediaQuery.of(context).size.width,
      child: Material(
          color: Colors.white.withOpacity(0.9),
          elevation: 5.0,
          shadowColor: Colors.black,
          borderRadius: BorderRadius.circular(14.0),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                    flex: 8,
                    fit: FlexFit.tight,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Flexible(
                            flex: 7,
                            fit: FlexFit.loose,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                GlobalWidgets.twoRowText(
                                    firstText: 'Asignatura: ',
                                    secondText: materia),
                                GlobalWidgets.twoRowText(
                                    firstText: 'Nota: ', secondText: nota),
                              ],
                            )),
                        Flexible(
                          flex: 3,
                          fit: FlexFit.tight,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: aprobado
                                ? CircleAvatar(
                                    minRadius: 24,
                                    maxRadius: 24,
                                    backgroundColor: Colors.green,
                                    child: Icon(
                                      Icons.check,
                                      color: Colors.white,
                                      size: 24,
                                    ),
                                  )
                                : Icon(
                                    Icons.cancel,
                                    color: Colors.redAccent,
                                    size: 50,
                                  ),
                          ),
                        ),
                      ],
                    )),
                Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: Center(
                      child: aprobado
                          ? Text(
                              'APROBADO',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: Fonts.primaryFont,
                                  fontSize: 22,
                                  color: Colors.green,
                                  fontWeight: FontWeight.bold),
                            )
                          : Text(
                              'REPROBADO',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: Fonts.primaryFont,
                                  fontSize: 22,
                                  color: Colors.redAccent,
                                  fontWeight: FontWeight.bold),
                            ),
                    ))
              ],
            ),
          )),
    );
  }

  static Widget createAvgWidget(
      {@required BuildContext context,
      @required String materia,
      @required String nota,
      @required bool aprobado}) {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 30, top: 20),
      height: 120,
      width: MediaQuery.of(context).size.width,
      child: Material(
          color: Colors.white.withOpacity(0.9),
          elevation: 5.0,
          shadowColor: Colors.black,
          borderRadius: BorderRadius.circular(14.0),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                    flex: 8,
                    fit: FlexFit.tight,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Flexible(
                            flex: 7,
                            fit: FlexFit.loose,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: GlobalWidgets.twoRowText(
                                      firstText: 'Asignatura: ',
                                      secondText: materia),
                                ),
                                Expanded(
                                  child: GlobalWidgets.twoRowText(
                                      firstText: 'Nota: ', secondText: nota),
                                )
                              ],
                            )),
                      ],
                    )),
                Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: Center(
                      child: aprobado
                          ? Text(
                              'APROBADO',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: Fonts.primaryFont,
                                  fontSize: 22,
                                  color: Colors.green,
                                  fontWeight: FontWeight.bold),
                            )
                          : nota == 'No publicado'
                              ? Text(
                                  'NO PUBLICADO'.toUpperCase(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: Fonts.primaryFont,
                                      fontSize: 22,
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold),
                                )
                              : Text(
                                  'REPROBADO',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: Fonts.primaryFont,
                                      fontSize: 22,
                                      color: Colors.redAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                    ))
              ],
            ),
          )),
    );
  }

  static Widget createReposicionWidget(
      {@required Subject subject,
      @required HomeBloc homeBloc,
      @required BuildContext context}) {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 30, top: 20),
      height: 110,
      width: MediaQuery.of(context).size.width,
      child: Material(
          color: Colors.white.withOpacity(0.9),
          elevation: 5.0,
          shadowColor: Colors.black,
          borderRadius: BorderRadius.circular(14.0),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Flexible(
                    flex: 8,
                    fit: FlexFit.loose,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GlobalWidgets.twoRowText(
                              firstText: 'Asignatura: ',
                              secondText: subject.subject),
                          GlobalWidgets.twoRowText(
                              firstText: 'Nota: ', secondText: subject.note),
                        ],
                      ),
                    )),
                Flexible(
                    flex: 3,
                    fit: FlexFit.loose,
                    child: Center(
                      child: GestureDetector(
                        onTap: () => homeBloc.add(DownloadRepositionForm(
                            link: subject.recoverLink,
                            recoverEnabled: subject.recoverEnabled)),
                        child: subject.recoverEnabled
                            ? Text(
                                'DESCARGAR RECUPERACION',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: Fonts.primaryFont,
                                    fontSize: 22,
                                    color: Color.fromRGBO(37, 117, 191, 1),
                                    fontWeight: FontWeight.bold),
                              )
                            : Text(
                                'RECUPERACIÓN INHABILITADA',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: Fonts.primaryFont,
                                    fontSize: 22,
                                    color: Colors.redAccent,
                                    fontWeight: FontWeight.bold),
                              ),
                      ),
                    ))
              ],
            ),
          )),
    );
  }

  static radioOption(
          {@required dynamic groupValue,
          @required dynamic value,
          Color activeColor,
          Color textColor,
          @required String labelText,
          double fontSize = 16,
          @required Function onChanged}) =>
      Row(
        children: [
          Flexible(
              child: Padding(
                padding: const EdgeInsets.only(right: 6.0),
                child: Radio(
                  activeColor: activeColor == null
                      ? Color.fromRGBO(37, 117, 191, .8)
                      : activeColor,
                  value: value,
                  groupValue: groupValue,
                  onChanged: (value) {
                    onChanged(value);
                  },
                ),
              ),
              flex: 1,
              fit: FlexFit.tight),
          Flexible(
            child: Text(
              labelText,
              style: TextStyle(
                fontSize: fontSize,
                fontFamily: Fonts.primaryFont,
                color: textColor == null ? Colors.black54 : textColor,
              ),
            ),
            flex: 3,
            fit: FlexFit.tight,
          ),
        ],
      );
}
